<?php

require_once('includes/conexion.php');
require_once("includes/generalfunctions.php");


///////// PROCESAR

//Metodo --> método de acceso de la petición
$metodo = strtolower($_SERVER["REQUEST_METHOD"]);

//Recurso --> recurso al que trata de acceder la petición
$uri = $_SERVER["REQUEST_URI"];

if($uri!=null){
    //divido el string en trozos mediante el delimitador elegido, similar a 'split()' en js
    $uri = explode("v1.0/",$uri)[1]; 
    $uri_array = explode("/",$uri);
    $recurso = array_shift($uri_array);
}



//Parámetros
if(count($_GET)!=null){
    $query_params = $_GET; //Parametros que vengan por query
}


if($_POST!=null){
        $query_params = $_POST; //Parametros que vengan por query
}
//if($_PUT != null){
//        $query_params = $_PUT;
//}

//Procesa peticiones PUT
if ($_SERVER['REQUEST_METHOD'] == 'PUT' || $_SERVER['REQUEST_METHOD'] == 'DELETE')
{

    $query_params = (array) json_decode(file_get_contents('php://input'));

}




$recurso = explode("?",$recurso)[0];
require('controllers/'.$recurso."_controller.php");
    


//////// RESPONDER

//Envío al buffer de salida una array
$output = array();

$output['metodo'] = $metodo;
$output['recurso'] = $recurso;

$output['response'] = $response;

echo json_encode($output);
