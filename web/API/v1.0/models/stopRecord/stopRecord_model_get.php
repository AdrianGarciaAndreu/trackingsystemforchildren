<?php

    error_reporting(E_ERROR | E_PARSE);


    $response = [];
    $patientId = 0;
    $command = "STOP_".strval($patientId);

    # Get the cameras address
    $sql = "SELECT *, COUNT(idCamera) as camerasNumber FROM cameras GROUP BY idCamera ";
    $res = mysqli_query($conexion, $sql);


    $operativeCameras = 0;
    $failCameras = 0;

    if(mysqli_num_rows($res)>0){
        while($rs = mysqli_fetch_assoc($res)){
            #array_push($response, $rs);

            # Open connections
            $host = $rs["addr"];
            $port = $rs["port"];


            
            $f = socket_create(AF_INET, SOCK_STREAM, SOL_TCP);
            if($f !=false){
                socket_set_option($f, SOL_SOCKET, SO_SNDTIMEO, array('sec' => 1, 'usec' => 500000));
                $s = socket_connect($f, $host, $port);
                if($s!=false){
                    $len = strlen($command);
                    
                    $operativeCameras++;

                    socket_sendto($f, $command, $len, 0, $host, $port);                    
                    socket_close($f);

                    session_start();
                    unset($_SESSION["recording"]);

                } else { $failCameras++;}
            } else { $failCameras++; }
            

        }
    }

    $response["succes"]=$operativeCameras;
    $response["fail"]=$failCameras;

?>
