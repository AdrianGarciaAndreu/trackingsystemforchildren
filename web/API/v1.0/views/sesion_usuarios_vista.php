<?php

//$response = $registros;

$output = "<table id='sesionActual_tabla' class='table table-striped table-borderless'>";

$output .= "<thead>";
    $output .= "<tr scope='row'>";
    $output .= "<th colspan='4'>Alumnos sesi&oacute;n actual</th>"; 
    $output .= "</tr>";
        
    $output .= "<tr scope='row'>";
    $output .= "<th>Grado</th><th>Nombre</th><th>Apellido</th><th>Momento</th>";
    $output .= "</tr>";
 $output .= "</thead>";
 $output .= "<tbody>";

foreach($registros as $valor){

    $output .= "<tr scope='row'>";
        $output .= "<td><svg height='20' width='20'><circle cx='10' cy='10' r='10' stroke='white' stroke-width='1' fill=".$valor["color"]." /></svg></td>";
        $output .= "<td>".$valor["Nombre"]."</td>";
        $output .= "<td>".$valor["Apellidos"]."</td>";
        $output .= "<td>".$valor["Momento"]."</td>";
    $output .= "</tr>";
}

if(sizeof($registros)<1){
    $output .= "<tr scope='row'>";
        $output .= "<td colspan='4'> No hay datos de la sesi&oacute;n actual </td>";
    $output .= "</tr>";
}

$output .= "</tbody>";

$output .= "</table>";


$response["tabla"] = $output;
?>