
api_uri = '/web/API/v1.0/';



function getUrlVars(){
    var vars = [], hash;
    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    for(var i = 0; i < hashes.length; i++)
    {
        hash = hashes[i].split('=');
        vars.push(hash[0]);
        vars[hash[0]] = hash[1];
    }
    return vars;
}




function loadPatientSessions(){
    fetch(api_uri+'sessionResultsAVG?idPatient='+getUrlVars()["idPatient"], {method: "GET"}).then(function(response) {
        return response.json();    
      }).then(function(myJson) {
        if (myJson.response){ 
            sessions = myJson.response;
            
                   
            avgSessionResults(sessions);
        }

      });
}





function avgSessionResults(results){

    let outputValues = [0, 0, 0, 0, 0, 0, 0];
    let sessionsLimit = 10;
    let counter = 0;

    
    if(results.length>0){

        for(result of results){

            if(counter<sessionsLimit){ 



                // Normalize the emotions from 0 to 100
                dataToDisplay=[result['happy'], result['surprise'], result['neutral'],
                result['disgust'],result['sad'],result['angry'],
                result['fear']];

                numberOfblocks = parseInt(result['analyzedFrames'])/parseInt(result['blockSize']);
                numberOfblocks = Math.ceil(numberOfblocks);
                
                let dataToDisplayNormalized = new Array();
                let emotionIndex = 0;
                for (emotion of dataToDisplay){
                    emotionRound = parseFloat(emotion);
                    dataToDisplayNormalized.push((emotionRound * 100)/ numberOfblocks);
                    
                    //console.log(numberOfblocks);

                    outputValues[emotionIndex] += (emotionRound * 100)/ numberOfblocks
                    emotionIndex++;
                }
                    

            }
            else{break;}
        
            counter++;
        }

        //Divide in the number of sessions used to compute the average
        let defOutputValues = new Array();
        for (emotion of outputValues){
            defOutputValues.push(emotion/counter);
        }

        populateData(defOutputValues);

    } else{

        document.getElementById("patient-stats-row").style.height = "100%";
        document.getElementById("patient-stats-row").style.alignItems = "center";

        document.getElementById("patient-stats").innerHTML = `<div class='h5'>No currently data available for this patient</div>`;
       
    }

}


function populateData(results){

    
    // Graphic
    labels = ['happy','surprise','neutral', 'disgust', 'sad','angry','fear'];
    

    data = {
      labels: labels,
      datasets: [{
        label: 'Emotions',
        data: results,
        spacing: 1,
        backgroundColor: [
          'rgba(255, 99, 132, 0.5)',
          'rgba(255, 159, 64, 0.5)',
          'rgba(255, 205, 86, 0.5)',
          'rgba(75, 192, 192, 0.5)',
          'rgba(54, 162, 235, 0.5)',
          'rgba(153, 102, 255, 0.5)',
          'rgba(201, 203, 207, 0.5)'
        ],
        hoverOffset: 3
      }]
    };
    
    let config = {
      type: 'doughnut',
        data: data,
        options: {
            responsive: true,
            maintainAspectRatio: false,
            plugins: {
                legend: {
                    display: true,
                    position: 'bottom',
                    labels:{
                        font:{
                            family: "Noto sans-serif",
                            size: 14

                        }
                    }
                },
                title:{ /*
                    display: true,
                    text: "Emotional analysis",
                    padding: { bottom:10,},
                    font:{
                        size: 20
                    }*/
                }
            }
        }
    };


    ctx = document.getElementById('chartAvg').getContext('2d');
    myChart = new Chart(ctx, config);

    
    document.getElementById("btn-sessions").style.display = "inline-flex";  


}





//////////////////////////////////////////////
//////////////////////////////////////////////






/**
 * Get user list to populate the selectable list
 */

 function getUsers(){
    fetch(api_uri+'user', {method: "GET"}).then(function(response) {
        return response.json();    
      }).then(function(myJson) {
        if (myJson.response){ 
            users = myJson.response;
            populateUserSelectable(users);
        }
  
      });
  }
  
  
  /**
  * Populates the selectable
  * @param {Users list} users 
  */
  function populateUserSelectable(users){
    
    selector = document.getElementById("input_therapist");
    for (user of users){
        if(user["id"]!=1){ // Avoid the admin 
  
            new_option = document.createElement("option");
            new_option.value = user["id"];
            new_option.text = user["name"]+" "+user["surname"];
  
            selector.add(new_option);
        }
  
    }
  
  
  }
  
  
  
  function checkFieldsForSubmitting(isEditting){
  
    var str_error = "";
    // Get all the fields
  
    var patientName = document.getElementById("input_name").value;
    if(patientName.length<=0){str_error += " You should introduce the patient's name.\n";}
    
    var patientSurname = document.getElementById("input_surname").value;
    if(patientSurname.length<=0){str_error += " You should introduce the patient's surname.\n";}
  
    var patientContactPerson = document.getElementById("input_personOfContact").value;
    if(patientContactPerson.length<=0){str_error += " You should introduce the patient's contact person name.\n";}
  
    var patientContactPhone = document.getElementById("input_contactPhone").value;
    if(patientContactPhone.length<=0){str_error += " You should introduce the patient's contact person phone number.\n";}
  
    var patientPathology= document.getElementById("input_pathology").value;
    if(patientPathology.length<=0){str_error += " You should introduce the patient's pathology.\n";}
  
  
    var patientImg = document.getElementById("input_img").files;
  
    if(!isEditting){
        if (patientImg.length<=0){str_error += " You should upload an image for the patient!"}
    } else{}
  
    var therapist= document.getElementById("input_therapist").value;
  
    if(str_error.length <=0){ // If no errors, submit the data to the server
        var patient = {}
        patient['name'] = patientName;
        patient['surname'] = patientSurname;
        patient['contactPerson'] = patientContactPerson;
        patient['contactPhone'] = patientContactPhone;
        patient['pathology'] = patientPathology;
        patient['therapist'] = therapist;
       
  
        if(isEditting){
            submitPatientEdit(patient);
        }else{
            console.log("newPatient");
            patient['img'] = patientImg[0];
            submitPatient(patient);
        }
        
  
    } else {
        alert(str_error);
    }
  
  }
  
  
  function submitPatient(patient){
    
    data = {
        name: patient["name"],
        surname: patient["surname"],
        contactPerson: patient["contactPerson"],
        contact: patient["contactPhone"],
        userInCharge: patient["therapist"],
        pathology: patient["pathology"],
    }
  
    var options = {
        method: "PUT",
        headers: {'Content-type': 'application/json; charset=UTF-8'
    },
        body: JSON.stringify(data)
    }
  
    fetch(api_uri+'patients', options).then(function(response){
        return response.json();
    }).then(function(JSONResponse){
        //console.log(JSONResponse);
        
        uploadPatientImg(JSONResponse.response, patient['img'])
        alert("Patient added succesfully!");
        
  
    });
  }
  
  
  /**
  * Edit the patient image
  * @param {Patient ID} patientId 
  * @param {Patient IMG} patientImg 
  */
  function uploadPatientImg(patientId, patientImg){
  
    var fdata = new FormData();
    fdata.append('id', patientId);
    fdata.append('img', patientImg);
  
  
    var options = {
        method: "POST",
        body: fdata
    }
    fetch(api_uri+'patientsImg', options).then(function(response){
        return response.json();
    }).then(function(JSONResponse){
        window.location.href = "home.html"        
    });
  
  }
  
  
  /**
  * Sends patient data to be edited on the server
  */
  function submitPatientEdit(patient){
  
  
    isNewImage = false;
    
    if(((document.getElementById("input_img").files)[0])){
        isNewImage = true;
        patient["img"] = (document.getElementById("input_img").files)[0];
    }
  
  
    console.log(patient);
  
    var fdata = new FormData();
    fdata.append('id', getUrlVars()["idPatient"]);
    fdata.append('name',patient["name"]);
    fdata.append('surname', patient["surname"]);
    fdata.append('contactPerson', patient["contactPerson"]);
    fdata.append('contact', patient["contactPhone"]);
    fdata.append('userInCharge', patient["therapist"]);
    fdata.append('pathology', patient["pathology"]);
    
    if(isNewImage){ 
        console.log("Image Append!");
        fdata.append('img', patient["img"]); 
    }
  
    var options = {
        method: "POST",
        body: fdata
    }
  
    fetch(api_uri+'patients', options).then(function(response){
        return response.json();
    }).then(function(JSONResponse){
        
        console.log(JSONResponse);
        alert("Pattiend updated successfully!");
        window.location.reload();
  
    });
  }
  
  
  
  
  
  /**
   * Opens the uploaded context
   */
   function openUploadContext(){
    document.getElementById("input_img").click();
  }
  
  
  
  /**
  * Updates the thumbnail image with the uploaded image.
  */
  function updateImage(){
    file = document.getElementById("input_img").files[0];
    document.getElementById("patient_new_img").src = URL.createObjectURL(file);
  }
  
  


  ///////////////////////////////////////
  function goToSessions(){
    window.location.href = "patientSessions.html?idPatient="+getUrlVars()["idPatient"];
  }



/**
 * Populate modal dialog with current user information
 */

 function populateModal(){

    patient = this.patient;
  
    document.getElementById("input_name").value = patient["name"];
    document.getElementById("input_surname").value = patient["surname"];
    document.getElementById("input_personOfContact").value = patient["contactPerson"];
    document.getElementById("input_contactPhone").value = patient["contact"];
    document.getElementById("input_pathology").value = patient["pathology"];
    document.getElementById("input_therapist").value = patient["userInCharge"];
  
    img = "noimg.svg";
    if(patient["img"].length>0){ img = patient["img"];} 

    document.getElementById("patient_new_img").src = "../img/patients/"+img;

    document.getElementById("modal-patients-save").setAttribute("onclick", "checkFieldsForSubmitting(true)");
    document.getElementById("modal-patients-save").innerHTML = "Edit patient";
  
  
  }


  /** 
 * Event to clear the modal on hide 
 */
$('#modal-patients').on('hidden.bs.modal', function () {
  
    document.getElementById("input_name").value ="";
    document.getElementById("input_surname").value = "";
    document.getElementById("input_personOfContact").value = "";
    document.getElementById("input_contactPhone").value = "";
    document.getElementById("input_pathology").value = "";
    //document.getElementById("input_therapist").value = 0;
  
    document.getElementById("input_img").files = null;
    document.getElementById("patient_new_img").src = "../img/patients/noimg.svg"
  
    document.getElementById("modal-patients-save").setAttribute("onclick", "checkFieldsForSubmitting(false)");
    document.getElementById("modal-patients-save").innerHTML = "Create patient";
  
  });
  
 

  function goBack(){
    history.back();
  }


function logOut(){

    fetch(api_uri+'logOut/', {method: "GET"
  }).then(function(response) {
      return response.json();
    }).then(function(myJson) {
      window.location.replace("../index.html")
  
  });
}
    