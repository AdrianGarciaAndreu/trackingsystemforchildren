api_uri = '/web/API/v1.0/';


fetch(api_uri+'logSession', {method: "GET"}).then(function(response) {
    return response.json();    
  }).then(function(myJson) {
    if (myJson.response){ 
        // Everything OK
    } else {
        // User non identified!!!
        window.location.replace("../index.html");
    }

  });



  function checkRecording(){

    fetch(api_uri+'recording', {method: "GET"}).then(function(response) {
      return response.json();    
    }).then(function(myJson) {
      console.log(myJson)
      if (myJson.response){ 
        recordRunning();
      } else { // Not recording
        recordNotRunning();
      }
  
    });

  }

  function recordRunning(){
    
    stopBtn = document.getElementById("record-stop");
    startBtn = document.getElementById("record-start");

    if(startBtn){      
      startBtn.classList.add("disabled");
      startBtn.disabled = true;
    }
    if(stopBtn){
      stopBtn.classList.remove("disabled");
      stopBtn.disabled = false;
    }

  }

  function recordNotRunning(){

    stopBtn = document.getElementById("record-stop");
    startBtn = document.getElementById("record-start");

    if(stopBtn){
      stopBtn.classList.add("disabled");
      stopBtn.disabled = true;
    }
    if(startBtn){
      startBtn.classList.remove("disabled");
      startBtn.disabled = false;
    }

  }


  checkRecording();

