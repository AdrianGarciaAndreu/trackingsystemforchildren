
api_uri = '/web/API/v1.0/'; // API URL
var patient;

/**
 * Gets personal patient data
 */
function loadPatientData(){

    fetch(api_uri+'patients?id='+getUrlVars()["idPatient"], {method: "GET"}).then(function(response) {
        return response.json();    
      }).then(function(myJson) {
        if (myJson.response){ 
            populatePatientData(myJson.response[0]);
        }

      });
    
}

/**
 * Populates the web elements with all the personal
 * patient data.
 * 
 * @param {Patient data} patient 
 */
function populatePatientData(patient){

    this.patient = patient;

    document.getElementById("patient-name").innerHTML = "<span class='h6'>"+patient["name"]+" "+patient["surname"]+"</span>";
    document.getElementById("patient-contactPerson").innerHTML = patient["contactPerson"];
    document.getElementById("patient-contactNumber").innerHTML = patient["contact"];
    document.getElementById("patient-therapist").innerHTML = patient["uname"]+" "+patient["usurname"];
    document.getElementById("patient-pathology").innerHTML = patient["pathology"];
    
    if(patient["img"].length>0){
        document.getElementById("patient-img").src = "../img/patients/"+patient["img"];
    }
    


}


/**
 * Starts a recording a session for the current patient
 */
function startRecording(){

    // START RECORDING LOCKING CONTROLS
    document.getElementById("record-stop").disable = true;
    document.getElementById("record-stop").classList.add("disable");
    document.getElementById("record-start").disable = true;
    document.getElementById("record-start").classList.add("disable");

    document.getElementById("loadScreen").style.display = "block"; //display load screen

    // STARTS COMM WITH THE SERVER
    fetch(api_uri+'startRecord/?idPatient='+getUrlVars()["idPatient"], {method: "GET"}).then(function(response) {
        return response.json();
    
      }).then(function(myJson) {

        success = parseInt(myJson.response["succes"]);
        document.getElementById("loadScreen").style.display = "none"; // hides load screen
        fail = parseInt(myJson.response["fail"]);
        if(success>0){
          if(fail>0){ 
            console.log("Some cameras don't work");
            checkRecording();
          }
        } else {
          notifyUser("NO CAMERAS WORKING!");
        }

      });
}

/**
 * Stop recording the current session
 */
function stopRecording(){


    // START RECORDING LOCKING CONTROLS
    document.getElementById("record-stop").disable = true;
    document.getElementById("record-stop").classList.add("disable");
    document.getElementById("record-start").disable = true;
    document.getElementById("record-start").classList.add("disable");

    document.getElementById("loadScreen").style.display = "block"; //display load screen

    // STARTS COMM WITH THE SERVER
    fetch(api_uri+'stopRecord/', {method: "GET"}).then(function(response) {
        return response.json();
    
      }).then(function(myJson) {

        success = parseInt(myJson.response["succes"]);
        document.getElementById("loadScreen").style.display = "none"; // hides load screen

        fail = parseInt(myJson.response["fail"]);
        if(success>0){
          if(fail>0){ 
            //notifyUser("Some cameras don't work");
            //console.log("Some cameras don't work")
            checkRecording();
          }
        } else {
          notifyUser("NO CAMERAS WORKING!");
        }

      });

}




/**
 * Creates an alert to inform the user
 */
function notifyUser(msg){
  alert(msg);
}
