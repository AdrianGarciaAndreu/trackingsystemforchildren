api_uri = '/web/API/v1.0/'



function LinkCheck(url)
{
    var http = new XMLHttpRequest();
    http.open('HEAD', url, false);
    try{
        http.send();
    }
    catch(err){
        console.log("Nothing");
     }

    return http.status!=404;
}

function getUrlVars(){
    var vars = [], hash;
    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    for(var i = 0; i < hashes.length; i++)
    {
        hash = hashes[i].split('=');
        vars.push(hash[0]);
        vars[hash[0]] = hash[1];
    }
    return vars;
}



/**
 * Get request to retrieve the current patients
 * of the system
 * @param {html container id, where to put the result ouput} container_id
 */
function getPatients(container_id){
  
  fetch(api_uri+'session?idPatient='+getUrlVars()["idPatient"], {method: "GET"
}).then(function(response) {
    return response.json();
  }).then(function(myJson) {
    if (myJson.response){ 
         updateSessionsGrid(myJson.response, container_id); 
    } 
         
  });
  
}


/**
 * Updates the the cards grid with
 * the patients content
 */
function updateSessionsGrid(response, container_id){

    rowCounter = 0;
    output = "";

    response.forEach((session, index) => {
      if (index % 4 == 0 ){ 
        rowCounter++;
        cnt = '<div id="sessionsContainer_'+rowCounter+'" class="col-12 d-flex p-2 justify-content-center" id="contentPanel"></div>';
        document.getElementById(container_id).innerHTML += cnt;
      }

        
       // if(patient["img"].length>0){  patientImg = `../img/patients/${patient["img"]}`;}
      //  else{patientImg = `../img/patients/noimg.svg`; }

        results = session["results"];
        cover = "";

        //console.log(session);

        avgTotal = 0;
        avgDuration = 0;
        avgIndex = 0;
        for (const result of results) {
          avgTotal += parseFloat(result["analyzedFrames"]*100)/result["totalFrames"];
        
          avgDuration = Math.round(result["totalFrames"]/result["blockSize"]);

          avgIndex++;
        }
         
          minutes = Math.floor(avgDuration / 60);
          seconds = avgDuration - minutes * 60;

          avgTotal = (avgTotal/parseFloat(avgIndex)).toFixed(2);

          cardBody = `
          <div class='row pt-2'>
            <div class='col-12'>
              <div>Record duration <br><b>${pad(minutes,2)}:${pad(seconds,2)}</b></div>
              <div>Detection accuaracy<br><b>${avgTotal}%</b></div>
            </div>
          </div>
          `

  
          card = `<div class='m-4 d-flex card grid-card' onclick='goToSession(${session["id"]})'>
                    <div class='card-header'>${session["sessionDate"]} session</div>
                    <div class='card-body text-center'>${cardBody}</div> 
                  </div>`;

          document.getElementById("sessionsContainer_"+rowCounter).innerHTML += card;



        
     });
}



function populateCards(session, img){

}


//////////////////////////////////////////////
//////////////////////////////////////////////






/**
 * Get user list to populate the selectable list
 */

 function getUsers(){
  fetch(api_uri+'user', {method: "GET"}).then(function(response) {
      return response.json();    
    }).then(function(myJson) {
      if (myJson.response){ 
          users = myJson.response;
          populateUserSelectable(users);
      }

    });
}


/**
* Populates the selectable
* @param {Users list} users 
*/
function populateUserSelectable(users){
  
  selector = document.getElementById("input_therapist");
  for (user of users){
      if(user["id"]!=1){ // Avoid the admin 

          new_option = document.createElement("option");
          new_option.value = user["id"];
          new_option.text = user["name"]+" "+user["surname"];

          selector.add(new_option);
      }

  }


}



function checkFieldsForSubmitting(isEditting){

  var str_error = "";
  // Get all the fields

  var patientName = document.getElementById("input_name").value;
  if(patientName.length<=0){str_error += " You should introduce the patient's name.\n";}
  
  var patientSurname = document.getElementById("input_surname").value;
  if(patientSurname.length<=0){str_error += " You should introduce the patient's surname.\n";}

  var patientContactPerson = document.getElementById("input_personOfContact").value;
  if(patientContactPerson.length<=0){str_error += " You should introduce the patient's contact person name.\n";}

  var patientContactPhone = document.getElementById("input_contactPhone").value;
  if(patientContactPhone.length<=0){str_error += " You should introduce the patient's contact person phone number.\n";}

  var patientPathology= document.getElementById("input_pathology").value;
  if(patientPathology.length<=0){str_error += " You should introduce the patient's pathology.\n";}


  var patientImg = document.getElementById("input_img").files;

  if(!isEditting){
      if (patientImg.length<=0){str_error += " You should upload an image for the patient!"}
  } else{}

  var therapist= document.getElementById("input_therapist").value;

  if(str_error.length <=0){ // If no errors, submit the data to the server
      var patient = {}
      patient['name'] = patientName;
      patient['surname'] = patientSurname;
      patient['contactPerson'] = patientContactPerson;
      patient['contactPhone'] = patientContactPhone;
      patient['pathology'] = patientPathology;
      patient['therapist'] = therapist;
     

      if(isEditting){
          submitPatientEdit(patient);
      }else{
          console.log("newPatient");
          patient['img'] = patientImg[0];
          submitPatient(patient);
      }
      

  } else {
      alert(str_error);
  }

}


function submitPatient(patient){
  
  data = {
      name: patient["name"],
      surname: patient["surname"],
      contactPerson: patient["contactPerson"],
      contact: patient["contactPhone"],
      userInCharge: patient["therapist"],
      pathology: patient["pathology"],
  }

  var options = {
      method: "PUT",
      headers: {'Content-type': 'application/json; charset=UTF-8'
  },
      body: JSON.stringify(data)
  }

  fetch(api_uri+'patients', options).then(function(response){
      return response.json();
  }).then(function(JSONResponse){
      //console.log(JSONResponse);
      
      uploadPatientImg(JSONResponse.response, patient['img'])
      alert("Patient added succesfully!");
      

  });
}


/**
* Edit the patient image
* @param {Patient ID} patientId 
* @param {Patient IMG} patientImg 
*/
function uploadPatientImg(patientId, patientImg){

  var fdata = new FormData();
  fdata.append('id', patientId);
  fdata.append('img', patientImg);


  var options = {
      method: "POST",
      body: fdata
  }
  fetch(api_uri+'patientsImg', options).then(function(response){
      return response.json();
  }).then(function(JSONResponse){
      window.location.href = "home.html"        
  });

}


/**
* Sends patient data to be edited on the server
*/
function submitPatientEdit(patient){


  isNewImage = false;
  
  if(((document.getElementById("input_img").files)[0])){
      isNewImage = true;
      patient["img"] = (document.getElementById("input_img").files)[0];
  }


  console.log(patient);

  var fdata = new FormData();
  fdata.append('id', getUrlVars()["idPatient"]);
  fdata.append('name',patient["name"]);
  fdata.append('surname', patient["surname"]);
  fdata.append('contactPerson', patient["contactPerson"]);
  fdata.append('contact', patient["contactPhone"]);
  fdata.append('userInCharge', patient["therapist"]);
  fdata.append('pathology', patient["pathology"]);
  
  if(isNewImage){ 
      console.log("Image Append!");
      fdata.append('img', patient["img"]); 
  }

  var options = {
      method: "POST",
      body: fdata
  }

  fetch(api_uri+'patients', options).then(function(response){
      return response.json();
  }).then(function(JSONResponse){
      
      console.log(JSONResponse);
      alert("Pattiend updated successfully!");
      window.location.reload();

  });
}





/**
 * Opens the uploaded context
 */
 function openUploadContext(){
  document.getElementById("input_img").click();
}



/**
* Updates the thumbnail image with the uploaded image.
*/
function updateImage(){
  file = document.getElementById("input_img").files[0];
  document.getElementById("patient_new_img").src = URL.createObjectURL(file);
}




///////////////////////////////////////


/**
 * Go to patient details page
 * @param {Patient id} patientID 
 */
 function goToSession(sessionID){
  //console.log("sessionDetails.html?idPatient="+getUrlVars()["idPatient"]+"&id="+(sessionID))
  window.location.href = "sessionDetails.html?idPatient="+getUrlVars()["idPatient"]+"&id="+(sessionID);
  //alert("Go to other page");
}



/**
* Populate modal dialog with current user information
*/

function populateModal(){

  patient = this.patient;

  document.getElementById("input_name").value = patient["name"];
  document.getElementById("input_surname").value = patient["surname"];
  document.getElementById("input_personOfContact").value = patient["contactPerson"];
  document.getElementById("input_contactPhone").value = patient["contact"];
  document.getElementById("input_pathology").value = patient["pathology"];
  document.getElementById("input_therapist").value = patient["userInCharge"];

  img = "noimg.svg";
  if(patient["img"].length>0){ img = patient["img"];} 

  document.getElementById("patient_new_img").src = "../img/patients/"+img;

  document.getElementById("modal-patients-save").setAttribute("onclick", "checkFieldsForSubmitting(true)");
  document.getElementById("modal-patients-save").innerHTML = "Edit patient";


}


/** 
* Event to clear the modal on hide 
*/
$('#modal-patients').on('hidden.bs.modal', function () {

  document.getElementById("input_name").value ="";
  document.getElementById("input_surname").value = "";
  document.getElementById("input_personOfContact").value = "";
  document.getElementById("input_contactPhone").value = "";
  document.getElementById("input_pathology").value = "";
  //document.getElementById("input_therapist").value = 0;

  document.getElementById("input_img").files = null;
  document.getElementById("patient_new_img").src = "../img/patients/noimg.svg"

  document.getElementById("modal-patients-save").setAttribute("onclick", "checkFieldsForSubmitting(false)");
  document.getElementById("modal-patients-save").innerHTML = "Create patient";

});



function goBack(){
  history.back();
}


function pad(num, size) {
  num = num.toString();
  while (num.length < size) num = "0" + num;
  return num;
}


function logOut(){

  fetch(api_uri+'logOut/', {method: "GET"
}).then(function(response) {
    return response.json();
  }).then(function(myJson) {
    window.location.replace("../index.html")

});
}