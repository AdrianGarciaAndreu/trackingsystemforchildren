api_uri = '/web/API/v1.0/'



// Starts a record
function startRecording(){
    //TODO: Start recording depending on the patient
    fetch(api_uri+'startRecord/', {method: "GET"}).then(function(response) {
        return response.json();
    
      }).then(function(myJson) {
          //document.getElementById("recordStop_btn").style.display = "block"
          //document.getElementById("recordStart_btn").style.display = "none"

          // Play animation

      });
}
    

// Stops the current recording
function stopRecording(){

    fetch(api_uri+'stopRecord/', {method: "GET"}).then(function(response) {
        return response.json();
    
      }).then(function(myJson) {
          //document.getElementById("recordStop_btn").style.display = "none"
          //document.getElementById("recordStart_btn").style.display = "block"

          // Stop animation
      });

}


function refreshSessionGrid(){
  
  fetch(api_uri+'session/?idPatient=1', {method: "GET"
}).then(function(response) {
    return response.json();

  }).then(function(myJson) {
    if (myJson.response){ 
      
      rowCounter = 0;
      output = "";

      myJson.response.forEach((session, index) => {
        if (index % 3 == 0 ){ 
          rowCounter++;
          cnt = '<div id="sessionsContainer_'+rowCounter+'" class="col-12 d-flex p-2 justify-content-center" id="contentPanel"></div>';
          document.getElementById("main_container").innerHTML += cnt;
        }

          console.log(session);
          cardHeader = `<div class='container-fluid'>
                          <div class='col-12'>${session["sessionDate"]}</div>
                          <div class='col-12'>${session["name"]},${session["surname"]}</div>
                        </div>`;
          cardBody = `<div class='col-12 p-2'><i style="font-size:2.5rem;" class="fa-solid fa-arrow-up-right-from-square"></i></div>`

          //cardContent = "adasdsa";
          card = "<div style='width:16rem' onmouseover=\"changeSize(this)\" onmouseleave=\"originalSize(this)\" class='m-5 d-flex card' onclick='goToSessionDetail("+(session["id"])+")'><div class='card-header text-center'>"+cardHeader+"</div> <div class='card-body text-center'> "+cardBody+"</div> </div>";
          document.getElementById("sessionsContainer_"+rowCounter).innerHTML += card;
          
       });
    } // End of the response processing

  });
  
}



function goToSessionDetail(sessionDetailID){
  window.location.href = "common/sessionDetail.html?id="+(sessionDetailID);
}


function changeSize(element){
  
  element.style.width="18rem";
  element.style.height="12rem";
  element.style.boxShadow ="5px 5px 5px #EEEEEEFF";
}

function originalSize(element){
  element.style.width="16rem";
  element.style.height="10rem";
  element.style.boxShadow ="none";

}