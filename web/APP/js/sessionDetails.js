
api_uri = '/web/API/v1.0/'; // API URL
var patient;



/**
 * Get the variables located in the URL 
 * @returns URL variables as an array
 */
function getUrlVars(){
    var vars = [], hash;
    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    for(var i = 0; i < hashes.length; i++)
    {
        hash = hashes[i].split('=');
        vars.push(hash[0]);
        vars[hash[0]] = hash[1];
    }
    return vars;
}


/**
 * Get the cameras which have ben used to record the current session
 */
function readSessionResultsCameras(){

    fetch(api_uri+'sessionCameras?idSession='+getUrlVars()["id"], {method: "GET"}).then(function(response) {
        return response.json();    
      }).then(function(myJson) {
        if (myJson.response){ 
            cameras = myJson.response;
            populateSelectableCameras(cameras)
        }

      });
}

/**
 * Populate the cameras selector of the page with the available cameras
 * used to record the session.
 * 
 * when the cameras are loaded the data of the current camera for the session
 * will be loaded too
 * @param {cameras list} cameras 
 */
function populateSelectableCameras(cameras){
   
    selector = document.getElementById("sessionDet_camList");
    
    firstCamera = 0;
    index = 0;
    for (camera of cameras){
        
        new_option = document.createElement("option");
        new_option.value = camera["idCamera"];
        new_option.text = camera["name"];
            
        selector.add(new_option);
        
        if(index==0){firstCamera = new_option.value;} // Keep the first camera ID
        index++;
    }

    // Check for previously selected cameras
   if(getUrlVars()["idCamera"]){
    selector.value = getUrlVars()["idCamera"];
    loadSessionDetails(getUrlVars()["idCamera"]);
   } else{
    loadSessionDetails(firstCamera);
   }


}



/**
 * Loads the obtained results from a specific camera record of 
 * the current session
 * 
 * @param {Camera of the session to load the specific data} idCamera 
 */
function loadSessionDetails(idCamera){


    //TODO: Get current camera to request the query

    fetch(api_uri+'sessionResults?idSession='+getUrlVars()["id"]+'&idCamera='+idCamera, {method: "GET"}).then(function(response) {
        return response.json();    
      }).then(function(myJson) {
        if (myJson.response){ 
            details = myJson.response;
            populateSeesionDetails(details);
        }

      });
}


/**
 * Populates the web content with all the data retrieved from the server side 
 * of the current camera record selected for the actual session
 * 
 * @param {Details of the session record} details 
 */
function populateSeesionDetails(details){
    


    titulo = `<span class="h4"> ${details["sessionDate"]} Session results</span>`;
    
    results = details["results"];

    source = document.createElement("source");
    source.setAttribute('src', '../'+results["record"]);
    source.setAttribute('type', 'video/mp4');
    
    
    // Normalize the emotions from 0 to 100
    dataToDisplay=[results['happy'], results['surprise'], results['neutral'],
    results['disgust'],results['sad'],results['angry'],
    results['fear']];

    numberOfblocks = parseInt(results['analyzedFrames'])/parseInt(results['blockSize']);
    numberOfblocks = Math.ceil(numberOfblocks);

    domminantEmotion = 0;
    dominantValue = 0;

    var dataToDisplayNormalized = new Array();
    var emotionIndex = 0;

    for (emotion of dataToDisplay){
        emotionRound = parseFloat(emotion);
        nValue = (emotionRound * 100)/ numberOfblocks;
      
        if (nValue >= dominantValue){ 
            dominantValue = nValue.toFixed(2); 
            domminantEmotion = emotionIndex;
        }

        dataToDisplayNormalized.push(nValue);
        emotionIndex++;
    }

    

    // START CHART
    labels = ['happy','surprise','neutral', 'disgust', 'sad','angry','fear'];


    data = {
      labels: labels,
      datasets: [{
        label: 'Emotions',
        data: dataToDisplayNormalized,
        spacing: 1,
        backgroundColor: [
          'rgba(255, 99, 132, 0.5)',
          'rgba(255, 159, 64, 0.5)',
          'rgba(255, 205, 86, 0.5)',
          'rgba(75, 192, 192, 0.5)',
          'rgba(54, 162, 235, 0.5)',
          'rgba(153, 102, 255, 0.5)',
          'rgba(201, 203, 207, 0.5)'
        ],
        hoverOffset: 3
      }]
    };
    
    let config = {
      type: 'doughnut',
        data: data,
        options: {
            responsive: true,
            maintainAspectRatio: false,
            plugins: {
                legend: {
                    display: true,
                    position: 'bottom',
                    labels:{
                        font:{
                            family: "Noto sans-serif",
                            size: 14
                
                        }
                    }
                },
                title:{
                    display: true,
                    text: "Emotional analysis",
                    padding: { bottom:10},
                    font:{
                        family: "Noto sans-serif",
                        size: 20
                    }
                }
            }
        }
    };




    ctx = document.getElementById('chartSession').getContext('2d');
    myChart = new Chart(ctx, config);
    // END CHART


    document.getElementById("sessionDet_domEmotionLbl").innerHTML = `Dominant emotion is <b>${labels[domminantEmotion]}</b>, 
    with <b>${dominantValue}%</b> of the total`;

    // Calculate the factor of the succesfully detected faces within the total ammount of record frames
    accuaracy = parseFloat((parseInt(results["analyzedFrames"])*100)/parseInt(results["totalFrames"])).toFixed(2);
    stats = `<b>${accuaracy}%</b> succesful detection rate`;


    document.getElementById("sessionDet_title").innerHTML = titulo;
    document.getElementById("sessionDet_record_video").appendChild(source);
    document.getElementById("sessionDet_accuaracyLbl").innerHTML = stats;

    getNextAndPreviousSessions();
}



/**
 * Get the next and previous records IDs to be able to move between them through the web controls
 */
function getNextAndPreviousSessions(){

    fetch(api_uri+'sessionNextAndPrevious?idSession='+getUrlVars()["id"], {method: "GET"}).then(function(response) {
        return response.json();    
      }).then(function(myJson) {
        if (myJson.response){ 
            JSONResponse = myJson.response;

            populateNextAndPreviousSessionControls(JSONResponse["prev"],JSONResponse["post"])
        }

      });
}


/**
 * Configure the controls to advance or go back to the next or previous session.
 * @param {Previos session ID} prevSession 
 * @param {Next session ID} nextSession 
 */
function populateNextAndPreviousSessionControls(prevSession, nextSession){
    
    newLocation = "sessionDetails.html?idPatient="+getUrlVars()["idPatient"]+"&id=";

    if(prevSession!=0){
        document.getElementById("sessionDet_btn_past").onclick= function(event){
            window.location.href = newLocation+prevSession;
        };

    } else {
        document.getElementById("sessionDet_btn_past").classList.add("disabled");
        document.getElementById("sessionDet_btn_past").disabled = true;
    }
    
    if(nextSession!=0){
        document.getElementById("sessionDet_btn_post").onclick=function(event){
            window.location.href = newLocation+nextSession;
        };

    } else {
        document.getElementById("sessionDet_btn_post").classList.add("disabled");
        document.getElementById("sessionDet_btn_post").disabled = true;
    }

}





///////////////////////////////////////////////
///////////////////////////////////////////////

/**
 * Gets personal patient data
 */
 function loadPatientData(){

    fetch(api_uri+'patients?id='+getUrlVars()["idPatient"], {method: "GET"}).then(function(response) {
        return response.json();    
      }).then(function(myJson) {
        if (myJson.response){ 
            populatePatientData(myJson.response[0]);
        }

      });
    
}


/**
 * 
 * Populates the web elements with all the personal
 * patient data.
 * 
 */
function populatePatientData(patient){

    this.patient = patient;

    document.getElementById("patient-name").innerHTML = "<b>"+patient["name"]+" "+patient["surname"]+"</b>";
    document.getElementById("patient-contactPerson").innerHTML = patient["contactPerson"];
    document.getElementById("patient-contactNumber").innerHTML = patient["contact"];
    document.getElementById("patient-therapist").innerHTML = patient["uname"]+" "+patient["usurname"];
    document.getElementById("patient-pathology").innerHTML = patient["pathology"];
    
    if(patient["img"].length>0){
        document.getElementById("patient-img").src = "../img/patients/"+patient["img"];
    }
    


}


/**
 * Starts a recording a session for the current patient
 */
function startRecording(){

    // START RECORDING LOCKING CONTROLS
    document.getElementById("record-stop").disable = true;
    document.getElementById("record-stop").classList.add("disable");
    document.getElementById("record-start").disable = true;
    document.getElementById("record-start").classList.add("disable");

    document.getElementById("loadScreen").style.display = "block"; //display load screen

    // STARTS COMM WITH THE SERVER
    fetch(api_uri+'startRecord/?idPatient='+getUrlVars()["idPatient"], {method: "GET"}).then(function(response) {
        return response.json();
    
      }).then(function(myJson) {

        success = parseInt(myJson.response["succes"]);
        document.getElementById("loadScreen").style.display = "none"; // hides load screen
        fail = parseInt(myJson.response["fail"]);
        if(success>0){
          if(fail>0){ 
            console.log("Some cameras don't work");
            checkRecording();
          }
        } else {
          notifyUser("NO CAMERAS WORKING!");
        }

      });
}

/**
 * Stop recording the current session
 */
function stopRecording(){

    // START RECORDING LOCKING CONTROLS
    document.getElementById("record-stop").disable = true;
    document.getElementById("record-stop").classList.add("disable");
    document.getElementById("record-start").disable = true;
    document.getElementById("record-start").classList.add("disable");
    
    document.getElementById("loadScreen").style.display = "block"; //display load screen

    // STARTS COMM WITH THE SERVER
    fetch(api_uri+'stopRecord/', {method: "GET"}).then(function(response) {
        return response.json();
    
      }).then(function(myJson) {
        success = parseInt(myJson.response["succes"]);
        document.getElementById("loadScreen").style.display = "none"; // hides load screen
        fail = parseInt(myJson.response["fail"]);
        if(success>0){
          if(fail>0){ 
            checkRecording();
          }
        } else {
          notifyUser("NO CAMERAS WORKING!");
        }

      });

}



/**
 * Populate modal dialog with current user information
 * to be edit
 */
 function populateModal(){

  patient = this.patient;

  document.getElementById("input_name").value = patient["name"];
  document.getElementById("input_surname").value = patient["surname"];
  document.getElementById("input_personOfContact").value = patient["contactPerson"];
  document.getElementById("input_contactPhone").value = patient["contact"];
  document.getElementById("input_pathology").value = patient["pathology"];
  document.getElementById("input_therapist").value = patient["userInCharge"];

  document.getElementById("patient_new_img").src = "../img/patients/"+patient["img"];

  document.getElementById("modal-patients-save").setAttribute("onclick", "checkFieldsForSubmitting(true)");
  document.getElementById("modal-patients-save").innerHTML = "Edit patient";

}



//////////////////////////////////////////////
//////////////////////////////////////////////







/**
 * Get user list to populate the selectable list
 */

 function getUsers(){
    fetch(api_uri+'user', {method: "GET"}).then(function(response) {
        return response.json();    
      }).then(function(myJson) {
        if (myJson.response){ 
            users = myJson.response;
            populateUserSelectable(users);
        }

      });
}


/**
 * Populates the selectable
 * @param {Users list} users 
 */
function populateUserSelectable(users){
    
    selector = document.getElementById("input_therapist");
    for (user of users){
        if(user["id"]!=1){ // Avoid the admin 

            new_option = document.createElement("option");
            new_option.value = user["id"];
            new_option.text = user["name"]+" "+user["surname"];

            selector.add(new_option);
        }

    }


}



function checkFieldsForSubmitting(isEditting){

    var str_error = "";
    // Get all the fields

    var patientName = document.getElementById("input_name").value;
    if(patientName.length<=0){str_error += " You should introduce the patient's name.\n";}
    
    var patientSurname = document.getElementById("input_surname").value;
    if(patientSurname.length<=0){str_error += " You should introduce the patient's surname.\n";}

    var patientContactPerson = document.getElementById("input_personOfContact").value;
    if(patientContactPerson.length<=0){str_error += " You should introduce the patient's contact person name.\n";}

    var patientContactPhone = document.getElementById("input_contactPhone").value;
    if(patientContactPhone.length<=0){str_error += " You should introduce the patient's contact person phone number.\n";}

    var patientPathology= document.getElementById("input_pathology").value;
    if(patientPathology.length<=0){str_error += " You should introduce the patient's pathology.\n";}


    var patientImg = document.getElementById("input_img").files;

    if(!isEditting){
        if (patientImg.length<=0){str_error += " You should upload an image for the patient!"}
    } else{}

    var therapist= document.getElementById("input_therapist").value;

    if(str_error.length <=0){ // If no errors, submit the data to the server
        var patient = {}
        patient['name'] = patientName;
        patient['surname'] = patientSurname;
        patient['contactPerson'] = patientContactPerson;
        patient['contactPhone'] = patientContactPhone;
        patient['pathology'] = patientPathology;
        patient['therapist'] = therapist;
       

        if(isEditting){
            submitPatientEdit(patient);
        }else{
            patient['img'] = patientImg[0];
            submitPatient(patient);
        }
        

    } else {
        alert(str_error);
    }

}


function submitPatient(patient){


    data = {
        name: patient["name"],
        surname: patient["surname"],
        contactPerson: patient["contactPerson"],
        contact: patient["contactPhone"],
        userInCharge: patient["therapist"],
        pathology: patient["pathology"],
    }

    var options = {
        method: "PUT",
        headers: {'Content-type': 'application/json; charset=UTF-8'
    },
        body: JSON.stringify(data)
    }

    fetch(api_uri+'patients', options).then(function(response){
        return response.json();
    }).then(function(JSONResponse){
        //console.log(JSONResponse);
        
        uploadPatientImg(JSONResponse.response, patient['img'])
        alert("Patient added succesfully!");
        

    });
}


/**
 * Edit the patient image
 * @param {Patient ID} patientId 
 * @param {Patient IMG} patientImg 
 */
function uploadPatientImg(patientId, patientImg){

    var fdata = new FormData();
    fdata.append('id', patientId);
    fdata.append('img', patientImg);


    var options = {
        method: "POST",
        body: fdata
    }
    fetch(api_uri+'patientsImg', options).then(function(response){
        return response.json();
    }).then(function(JSONResponse){
        window.location.href = "home.html"        
    });

}


/**
 * Sends patient data to be edited on the server
 */
function submitPatientEdit(patient){


    isNewImage = false;
    
    if(((document.getElementById("input_img").files)[0])){
        isNewImage = true;
        patient["img"] = (document.getElementById("input_img").files)[0];
    }


    console.log(patient);

    var fdata = new FormData();
    fdata.append('id', getUrlVars()["idPatient"]);
    fdata.append('name',patient["name"]);
    fdata.append('surname', patient["surname"]);
    fdata.append('contactPerson', patient["contactPerson"]);
    fdata.append('contact', patient["contactPhone"]);
    fdata.append('userInCharge', patient["therapist"]);
    fdata.append('pathology', patient["pathology"]);
    
    if(isNewImage){ 
        console.log("Image Append!");
        fdata.append('img', patient["img"]); 
    }

    var options = {
        method: "POST",
        body: fdata
    }

    fetch(api_uri+'patients', options).then(function(response){
        return response.json();
    }).then(function(JSONResponse){
        
        console.log(JSONResponse);
        alert("Pattiend updated successfully!");
        window.location.reload();

    });
}







/**
 * Opens the uploaded context
 */
function openUploadContext(){
    document.getElementById("input_img").click();
  }



/**
 * Updates the thumbnail image with the uploaded image.
 */
function updateImage(){
    file = document.getElementById("input_img").files[0];
    document.getElementById("patient_new_img").src = URL.createObjectURL(file);
}













/**
 * On change the camera, the session results that are displayed will change
 * @param {Selector with the cameras list} selector 
 */
function changeCameraEvent(selector){
    window.location.href = "sessionDetails.html?idPatient="+getUrlVars()["idPatient"]+"&id="+getUrlVars()["id"]+"&idCamera="+selector.value;
}





/** 
 * Event to clear the modal on hide 
 */
 $('#modal-patients').on('hidden.bs.modal', function () {
  
    document.getElementById("input_name").value ="";
    document.getElementById("input_surname").value = "";
    document.getElementById("input_personOfContact").value = "";
    document.getElementById("input_contactPhone").value = "";
    document.getElementById("input_pathology").value = "";
    //document.getElementById("input_therapist").value = 0;
  
    document.getElementById("input_img").files = null;
    document.getElementById("patient_new_img").src = "../img/patients/noimg.svg"
  
    document.getElementById("modal-patients-save").setAttribute("onclick", "checkFieldsForSubmitting(false)");
    document.getElementById("modal-patients-save").innerHTML = "Create patient";
  
  });
  

/**
 * Creates an alert to inform the user
 */
 function notifyUser(msg){
    alert(msg);
  }
  



  function goBack(){
    history.back();
  }


  function logOut(){

    fetch(api_uri+'logOut/', {method: "GET"
  }).then(function(response) {
      return response.json();
    }).then(function(myJson) {
      window.location.replace("../index.html")
  
  });
}