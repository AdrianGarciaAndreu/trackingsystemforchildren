api_uri = '/web/API/v1.0/'


function LinkCheck(url)
{
    var http = new XMLHttpRequest();
    http.open('HEAD', url, false);
    try{
        http.send();
    }
    catch(err){
        console.log("Nothing");
     }

    return http.status!=404;
}


/**
 * Get request to retrieve the current patients
 * of the system
 * @param {html container id, where to put the result ouput} container_id
 */
function getPatients(container_id){
  
  fetch(api_uri+'patientsList/', {method: "GET"
}).then(function(response) {
    return response.json();
  }).then(function(myJson) {
    if (myJson.response){  updatePatientsGrid(myJson.response, container_id); } // End of the response processing
  });
  
}


/**
 * Updates the the cards grid with
 * the patients content
 */
function updatePatientsGrid(response, container_id){

    rowCounter = 0;
    output = "";

    response.forEach((patient, index) => {
      if (index % 4 == 0 ){ 
        rowCounter++;
        cnt = '<div id="sessionsContainer_'+rowCounter+'" class="col-12 d-flex p-2 justify-content-center" id="contentPanel"></div>';
        document.getElementById(container_id).innerHTML += cnt;
      }


        if(patient["img"].length>0){  patientImg = `../img/patients/${patient["img"]}`;}
        else{patientImg = `../img/patients/noimg.svg`; }



        var fname = patient["name"]+" "+patient["surname"];
        if((fname).length>20){
          fname = fname.slice(0, 17);
          fname += "...";
        }

        cardBody = `
        <div class='row pt-2'>
          <div class='col-12'>
            <img src="${patientImg}" class="card-patient-img" alt="card-img-top">
          </div>
        </div>
        `

        card = "<div class='m-4 d-flex card grid-card' onclick='goToPatient("+(patient["id"])+")'><div class='card-header'>"+fname+"</div><div class='card-body text-center'> "+cardBody+"</div> </div>";
        document.getElementById("sessionsContainer_"+rowCounter).innerHTML += card;
        
     });
}



/**
 * Go to patient details page
 * @param {Patient id} patientID 
 */
function goToPatient(patientID){
  window.location.href = "patient.html?idPatient="+(patientID);
  //alert("Go to other page");
}




//////////////////////////////////////////////
//////////////////////////////////////////////







/**
 * Get user list to populate the selectable list
 */

 function getUsers(){
  fetch(api_uri+'user', {method: "GET"}).then(function(response) {
      return response.json();    
    }).then(function(myJson) {
      if (myJson.response){ 
          users = myJson.response;
          populateUserSelectable(users);
      }

    });
}


/**
* Populates the selectable
* @param {Users list} users 
*/
function populateUserSelectable(users){
  
  selector = document.getElementById("input_therapist");
  for (user of users){
      if(user["id"]!=1){ // Avoid the admin 

          new_option = document.createElement("option");
          new_option.value = user["id"];
          new_option.text = user["name"]+" "+user["surname"];

          selector.add(new_option);
      }

  }


}



function checkFieldsForSubmitting(isEditting){

  var str_error = "";
  // Get all the fields

  var patientName = document.getElementById("input_name").value;
  if(patientName.length<=0){str_error += " You should introduce the patient's name.\n";}
  
  var patientSurname = document.getElementById("input_surname").value;
  if(patientSurname.length<=0){str_error += " You should introduce the patient's surname.\n";}

  var patientContactPerson = document.getElementById("input_personOfContact").value;
  if(patientContactPerson.length<=0){str_error += " You should introduce the patient's contact person name.\n";}

  var patientContactPhone = document.getElementById("input_contactPhone").value;
  if(patientContactPhone.length<=0){str_error += " You should introduce the patient's contact person phone number.\n";}

  var patientPathology= document.getElementById("input_pathology").value;
  if(patientPathology.length<=0){str_error += " You should introduce the patient's pathology.\n";}


  var patientImg = document.getElementById("input_img").files;

  if(!isEditting){
      if (patientImg.length<=0){str_error += " You should upload an image for the patient!"}
  } else{}

  var therapist= document.getElementById("input_therapist").value;

  if(str_error.length <=0){ // If no errors, submit the data to the server
      var patient = {}
      patient['name'] = patientName;
      patient['surname'] = patientSurname;
      patient['contactPerson'] = patientContactPerson;
      patient['contactPhone'] = patientContactPhone;
      patient['pathology'] = patientPathology;
      patient['therapist'] = therapist;
     

      if(isEditting){
          submitPatientEdit(patient);
      }else{
          patient['img'] = patientImg[0];
          submitPatient(patient);
      }
      

  } else {
      alert(str_error);
  }

}


function submitPatient(patient){


  data = {
      name: patient["name"],
      surname: patient["surname"],
      contactPerson: patient["contactPerson"],
      contact: patient["contactPhone"],
      userInCharge: patient["therapist"],
      pathology: patient["pathology"],
  }

  var options = {
      method: "PUT",
      headers: {'Content-type': 'application/json; charset=UTF-8'
  },
      body: JSON.stringify(data)
  }

  fetch(api_uri+'patients', options).then(function(response){
      return response.json();
  }).then(function(JSONResponse){
      //console.log(JSONResponse);
      
      uploadPatientImg(JSONResponse.response, patient['img'])
      alert("Patient added succesfully!");
      

  });
}


/**
* Edit the patient image
* @param {Patient ID} patientId 
* @param {Patient IMG} patientImg 
*/
function uploadPatientImg(patientId, patientImg){

  var fdata = new FormData();
  fdata.append('id', patientId);
  fdata.append('img', patientImg);


  var options = {
      method: "POST",
      body: fdata
  }
  fetch(api_uri+'patientsImg', options).then(function(response){
      return response.json();
  }).then(function(JSONResponse){
      window.location.href = "home.html"        
  });

}


/**
* Sends patient data to be edited on the server
*/
function submitPatientEdit(patient){


  isNewImage = false;
  
  if(((document.getElementById("input_img").files)[0])){
      isNewImage = true;
      patient["img"] = (document.getElementById("input_img").files)[0];
  }


  console.log(patient);

  var fdata = new FormData();
  fdata.append('id', getUrlVars()["idPatient"]);
  fdata.append('name',patient["name"]);
  fdata.append('surname', patient["surname"]);
  fdata.append('contactPerson', patient["contactPerson"]);
  fdata.append('contact', patient["contactPhone"]);
  fdata.append('userInCharge', patient["therapist"]);
  fdata.append('pathology', patient["pathology"]);
  
  if(isNewImage){ 
      console.log("Image Append!");
      fdata.append('img', patient["img"]); 
  }

  var options = {
      method: "POST",
      body: fdata
  }

  fetch(api_uri+'patients', options).then(function(response){
      return response.json();
  }).then(function(JSONResponse){
      
      console.log(JSONResponse);
      alert("Pattiend updated successfully!");
      //window.location.href = "home.html";

  });
}




/**
 * Opens the uploaded context
 */
 function openUploadContext(){
  document.getElementById("input_img").click();
}



/**
* Updates the thumbnail image with the uploaded image.
*/
function updateImage(){
  file = document.getElementById("input_img").files[0];
  document.getElementById("patient_new_img").src = URL.createObjectURL(file);
}



function logOut(){

  fetch(api_uri+'logOut/', {method: "GET"
}).then(function(response) {
    return response.json();
  }).then(function(myJson) {
    window.location.replace("../index.html")

});
  

}
