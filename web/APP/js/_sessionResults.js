
api_uri = '/web/API/v1.0/';

function getUrlVars()
{
    var vars = [], hash;
    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    for(var i = 0; i < hashes.length; i++)
    {
        hash = hashes[i].split('=');
        vars.push(hash[0]);
        vars[hash[0]] = hash[1];
    }
    return vars;
}



function loadSessionResults(){

    
    fetch(api_uri+'sessionResults?idSession='+getUrlVars()["id"], {method: "GET"}).then(function(response) {
        return response.json();
    
      }).then(function(myJson) {
        if (myJson.response){ 
            sessions = myJson.response;
            
            for( session of sessions){
                console.log(session);
                populateData(session)
            }
        }

      });
}


function populateData(analysisData){

    // Common data
    patient = "Patient: <b>"+analysisData["name"]+" "+analysisData["surname"]+"</b>";
    sessionDate = "Date: "+analysisData["sessionDate"];

    detectAccuaracy = (analysisData["analyzedFrames"]/analysisData["totalFrames"]).toFixed(3);


    document.getElementById("lbl_title").innerHTML = patient;
    document.getElementById("lbl_subtitle").innerHTML = sessionDate+"</br> Detection accuaracy: "+(detectAccuaracy.toString())+"%";


    // Graphic
    labels = ['happy','surprise','neutral', 'disgust', 'sad','angry','fear'];
    
    dataToDisplay=[analysisData['happy'], analysisData['surprise'], analysisData['neutral'],
    analysisData['disgust'],analysisData['sad'],analysisData['angry'],
    analysisData['fear']] 


    // Normalize the emotions from 0 to 100
    numberOfblocks = parseInt(analysisData['analyzedFrames'])/parseInt(analysisData['blockSize']);
    numberOfblocks = Math.ceil(numberOfblocks);
    
    let dataToDisplayNormalized = new Array();
    for (emotion of dataToDisplay){
        
        dataToDisplayNormalized.push((emotion * 100)/ numberOfblocks);
    }
    
    

    data = {
      labels: labels,
      datasets: [{
        label: 'Emotions',
        data: dataToDisplayNormalized,
        spacing: 1,
        backgroundColor: [
          'rgba(255, 99, 132, 0.5)',
          'rgba(255, 159, 64, 0.5)',
          'rgba(255, 205, 86, 0.5)',
          'rgba(75, 192, 192, 0.5)',
          'rgba(54, 162, 235, 0.5)',
          'rgba(153, 102, 255, 0.5)',
          'rgba(201, 203, 207, 0.5)'
        ],
        hoverOffset: 3
      }]
    };
    
    
    
    
    let config = {
      type: 'doughnut',
        data: data,
        options: {
            scales: { 
            }
        }
    };


    ctx = document.getElementById('myChart').getContext('2d');
    myChart = new Chart(ctx, config);


}