api_uri = '/web/API/v1.0/';



/**
 * Check fields to perform logging
 */
function checkForLogin(){

    var user = document.getElementById("input-user").value;
    var pass = document.getElementById("input-pass").value;

    stdError = "";

    if(user.length<1){ stdError += "User or Email field is empty, please introduce it.\n"; }
    if(pass.length<1){ stdError += "Password field is empty, please introduce it.\n"; }

    if(stdError.length>0){ alert(stdError);} 
    else{ checkLogin(user, pass);}



}


function checkLogin(user, pass){

    
    fetch(api_uri+'login?user='+user+"&pass="+pass, {method: "GET"}).then(function(response) {
        return response.json();    
      }).then(function(myJson) {
        if (myJson.response){ 
            user = myJson.response;
            logingResolution(user);
        } else {
            alert("User incorrect! \nplease train again or contact with an administrator.");
        }

      });

}



function logingResolution(resolution){
    window.location.href = "common/home.html";
}