api_uri = '/web/API/v1.0/'



/**
 * Get user list to populate the selectable list
 */

 function getUsers(){
    fetch(api_uri+'user', {method: "GET"}).then(function(response) {
        return response.json();    
      }).then(function(myJson) {
        if (myJson.response){ 
            users = myJson.response;
            populateUserSelectable(users);
            populateUsersTable(users);
        }
  
      });
  }
  






function populateUsersTable(users){

    var tbodyResult = ``;
    for(user of users){
        tbodyResult += 
        `<tr>
            <td>${user["name"]}</td>
            <td>${user["surname"]}</td>
            <td>${user["mail"]}</td>
            <td>
                <div class='row'>
                    <div class='col-12'>
                        <a href='#' data-toggle="modal" data-target="#modal-users" onclick="getUserToEdit(${user["id"]})" class='ctm-btn ctm-a p-1'><i class="fa-solid fa-pen-to-square"></i></a>
                        <a href='#' onclick="deleteUser(${user["id"]},'${user["name"]}')" class='ctm-btn ctm-a p-1'><i class="fa-solid fa-trash-can"></i></a>
                    </div>
                </div>
            </td>
        <tr>`;
    }

    document.getElementById("tbody-users").innerHTML = tbodyResult;

}


function getCameras(){

    fetch(api_uri+'camera', {method: "GET"}).then(function(response) {
        return response.json();    
      }).then(function(myJson) {
        if (myJson.response){ 
            users = myJson.response;
            populateCamerasTable(users);
        }
  
      });

}

function populateCamerasTable(cameras){

    var tbodyResult = ``;
    for(camera of cameras){
        tbodyResult += 
        `<tr>
            <td>${camera["name"]}</td>
            <td>${camera["addr"]}</td>
            <td>${camera["port"]}</td>
            <td>
                <div class='row'>
                    <div class='col-12'><a href='#' data-toggle="modal" data-target="#modal-cameras" onclick="getCameraToEdit(${camera["idCamera"]})" class='ctm-btn ctm-a p-1'><i class="fa-solid fa-pen-to-square"></i></a>
                    <a href='#' onclick="deleteCamera(${camera["idCamera"]},'${camera["name"]}')" class='ctm-btn ctm-a p-1'><i class="fa-solid fa-trash-can"></i></a>
                    </div>
                </div>
            </td>
        <tr>`;
    }

    document.getElementById("tbody-cameras").innerHTML = tbodyResult;

}



  
  /**
  * Populates the selectable
  * @param {Users list} users 
  */
  function populateUserSelectable(users){
    
    selector = document.getElementById("input_therapist");
    for (user of users){
        if(user["id"]!=1){ // Avoid the admin 
  
            new_option = document.createElement("option");
            new_option.value = user["id"];
            new_option.text = user["name"]+" "+user["surname"];
  
            selector.add(new_option);
        }
  
    }
  
  
  }
  
  
  
  function checkFieldsForSubmitting(isEditting){
  
    var str_error = "";
    // Get all the fields
  
    var patientName = document.getElementById("input_name").value;
    if(patientName.length<=0){str_error += " You should introduce the patient's name.\n";}
    
    var patientSurname = document.getElementById("input_surname").value;
    if(patientSurname.length<=0){str_error += " You should introduce the patient's surname.\n";}
  
    var patientContactPerson = document.getElementById("input_personOfContact").value;
    if(patientContactPerson.length<=0){str_error += " You should introduce the patient's contact person name.\n";}
  
    var patientContactPhone = document.getElementById("input_contactPhone").value;
    if(patientContactPhone.length<=0){str_error += " You should introduce the patient's contact person phone number.\n";}
  
    var patientPathology= document.getElementById("input_pathology").value;
    if(patientPathology.length<=0){str_error += " You should introduce the patient's pathology.\n";}
  
  
    var patientImg = document.getElementById("input_img").files;
  
    if(!isEditting){
        if (patientImg.length<=0){str_error += " You should upload an image for the patient!"}
    } else{}
  
    var therapist= document.getElementById("input_therapist").value;
  
    if(str_error.length <=0){ // If no errors, submit the data to the server
        var patient = {}
        patient['name'] = patientName;
        patient['surname'] = patientSurname;
        patient['contactPerson'] = patientContactPerson;
        patient['contactPhone'] = patientContactPhone;
        patient['pathology'] = patientPathology;
        patient['therapist'] = therapist;
       
  
        if(isEditting){
            submitPatientEdit(patient);
        }else{
            patient['img'] = patientImg[0];
            submitPatient(patient);
        }
        
  
    } else {
        alert(str_error);
    }
  
  }
  
  
  function submitPatient(patient){
  
  
    data = {
        name: patient["name"],
        surname: patient["surname"],
        contactPerson: patient["contactPerson"],
        contact: patient["contactPhone"],
        userInCharge: patient["therapist"],
        pathology: patient["pathology"],
    }
  
    var options = {
        method: "PUT",
        headers: {'Content-type': 'application/json; charset=UTF-8'
    },
        body: JSON.stringify(data)
    }
  
    fetch(api_uri+'patients', options).then(function(response){
        return response.json();
    }).then(function(JSONResponse){
        //console.log(JSONResponse);
        
        uploadPatientImg(JSONResponse.response, patient['img'])
        alert("Patient added succesfully!");
        
  
    });
  }
  
  
  /**
  * Edit the patient image
  * @param {Patient ID} patientId 
  * @param {Patient IMG} patientImg 
  */
  function uploadPatientImg(patientId, patientImg){
  
    var fdata = new FormData();
    fdata.append('id', patientId);
    fdata.append('img', patientImg);
  
  
    var options = {
        method: "POST",
        body: fdata
    }
    fetch(api_uri+'patientsImg', options).then(function(response){
        return response.json();
    }).then(function(JSONResponse){
        window.location.href = "home.html"        
    });
  
  }
  
  
  /**
  * Sends patient data to be edited on the server
  */
  function submitPatientEdit(patient){
  
  
    isNewImage = false;
    
    if(((document.getElementById("input_img").files)[0])){
        isNewImage = true;
        patient["img"] = (document.getElementById("input_img").files)[0];
    }
  
  
    console.log(patient);
  
    var fdata = new FormData();
    fdata.append('id', getUrlVars()["idPatient"]);
    fdata.append('name',patient["name"]);
    fdata.append('surname', patient["surname"]);
    fdata.append('contactPerson', patient["contactPerson"]);
    fdata.append('contact', patient["contactPhone"]);
    fdata.append('userInCharge', patient["therapist"]);
    fdata.append('pathology', patient["pathology"]);
    
    if(isNewImage){ 
        console.log("Image Append!");
        fdata.append('img', patient["img"]); 
    }
  
    var options = {
        method: "POST",
        body: fdata
    }
  
    fetch(api_uri+'patients', options).then(function(response){
        return response.json();
    }).then(function(JSONResponse){
        
        console.log(JSONResponse);
        alert("Pattiend updated successfully!");
        //window.location.href = "home.html";
  
    });
  }
  
  


  function checkFieldsForSubmittingUser(isEditting, userId){
  
    var str_error = "";
    // Get all the fields
  
    var userName = document.getElementById("input-user-name").value;
    if(userName.length<=0){str_error += " You should introduce the patient's name.\n";}
    
    var userSurname = document.getElementById("input-user-surname").value;
    if(userSurname.length<=0){str_error += " You should introduce the patient's surname.\n";}
  
    var userEmail = document.getElementById("input-user-email").value;
    if(userEmail.length<=0){str_error += " You should introduce the user's email.\n";}
  
    var userPass = document.getElementById("input-user-pass").value;
    if(userPass.length<=0 && !isEditting){str_error += " You should introduce a password.\n";}
  
    var userPassRepeat= document.getElementById("input-user-pass-repeat").value;
    if(userPassRepeat.length<=0 && !isEditting){str_error += " You should repeat the password.\n";}
  
    if(userPass.length>0){
        if(userPass!=userPassRepeat) {
            str_error += " The passwords should coincide! \n";
        }
    }
  
  
    if(str_error.length <=0){ // If no errors, submit the data to the server
        var user = {}
        user['name'] = userName;
        user['surname'] = userSurname;
        user['mail'] = userEmail;

        if(userPass.length>0){
            user['pass'] = userPass;
        }
  
        if(isEditting){
            user["id"] = userId;
            submitUserEdit(user);
        }else{
            submitUser(user);
        }
        
  
    } else {
        alert(str_error);
    }
  
  }


  function submitUser(user){

  
    data = {
        name: user["name"],
        surname: user["surname"],
        mail: user["mail"],
        pass: user["pass"]
    }
  
    var options = {
        method: "PUT",
        headers: {'Content-type': 'application/json; charset=UTF-8'
    },
        body: JSON.stringify(data)
    }
  
    fetch(api_uri+'user', options).then(function(response){
        return response.json();
    }).then(function(JSONResponse){        
        alert("User added succesfully!");

        location.reload();
        
  
    });

  }


  function submitUserEdit(user){
    
    var fdata = new FormData();
    fdata.append('id', user["id"]);
    fdata.append('name',user["name"]);
    fdata.append('surname', user["surname"]);
    fdata.append('mail', user["mail"]);
    if(user["pass"]){
        fdata.append('pass', user["pass"]);
    }

    var options = {
        method: "POST",
        body: fdata
    }
  
    fetch(api_uri+'user', options).then(function(response){
        return response.json();
    }).then(function(JSONResponse){
        
        alert("User updated successfully!");
        location.reload();
  
    });

  }


  
function getUserToEdit(userId){

    fetch(api_uri+'user?id='+userId, {method: "GET"}).then(function(response) {
        return response.json();    
      }).then(function(myJson) {
        if (myJson.response){
            user = myJson.response[0];

            populateUserModal(user)
        }
  
      });
}


function deleteCamera(camId, camName){
   

    if(confirm("Are you sure you want to delete the camera '"+camName+"' ?")){
        data = {
            idCamera: camId
        }
    
        var options = {
            method: "DELETE",
            headers: {'Content-type': 'application/json; charset=UTF-8'
        },
            body: JSON.stringify(data)
        }
    
        fetch(api_uri+'camera', options).then(function(response){
            return response.json();
        }).then(function(JSONResponse){        
            alert("Camera deleted");
            location.reload();
        });

    }
}

function deleteUser(userId, userName){

    if(confirm("Are you sure you want to delete the user '"+userName+"' ?")){
        data = {
            id: userId
        }
    
        var options = {
            method: "DELETE",
            headers: {'Content-type': 'application/json; charset=UTF-8'
        },
            body: JSON.stringify(data)
        }
    
        fetch(api_uri+'user', options).then(function(response){
            return response.json();
        }).then(function(JSONResponse){        
            alert("User disabled");
            location.reload();
        });

    }

}




function checkFieldsForSubmittingCamera(isEditting, camId){
  
    var str_error = "";
    // Get all the fields
  
    var camName = document.getElementById("input-cameras-name").value;
    if(camName.length<=0){str_error += " You should introduce the camera's name.\n";}
    
    var camAddress = document.getElementById("input-cameras-address").value;
    if(camAddress.length<=0){str_error += " You should introduce the camera's IP Address.\n";}
  
    var camPort = document.getElementById("input-cameras-port").value;
    if(camPort.length<=0){str_error += " You should introduce the camera's port.\n";}

  
  
    if(str_error.length <=0){ // If no errors, submit the data to the server
        var camera = {}
        camera['name'] = camName;
        camera['address'] = camAddress;
        camera['port'] = camPort;

  
        if(isEditting){
            
            camera["id"] = camId;
            console.log(camera);
            submitCameraEdit(camera);
        }else{
            submitCamera(camera);
        }
        
  
    } else {
        alert(str_error);
    }
  
  }


/**
 * Submits a camera to be created
 * @param {Camera} camera 
 */
function submitCamera(camera){

    console.log(camera);
    data = {
        name: camera["name"],
        addr: camera["address"],
        port: camera["port"],
    }
  
    var options = {
        method: "PUT",
        headers: {'Content-type': 'application/json; charset=UTF-8'
    },
        body: JSON.stringify(data)
    }
  
    fetch(api_uri+'camera', options).then(function(response){
        return response.json();
    }).then(function(JSONResponse){  
        //console.log(JSONResponse);      
        alert("Camera added succesfully!");

        location.reload();
        
  
    });

}


/**
 * Sumbits a camera to be edited on the server
 * @param {Camera} camera 
 */
function submitCameraEdit(camera){

    console.log(camera)
    var fdata = new FormData();
    fdata.append('idCamera', camera["id"]);
    fdata.append('name',camera["name"]);
    fdata.append('addr', camera["address"]);
    fdata.append('port', camera["port"]);

    

    var options = {
        method: "POST",
        body: fdata
    }
  
    fetch(api_uri+'camera', options).then(function(response){
        return response.json();
    }).then(function(JSONResponse){
        
        console.log(JSONResponse);
        alert("Camera updated successfully!");
        location.reload();
  
    });

}


function getCameraToEdit(cameraId){

    fetch(api_uri+'camera?idCamera='+cameraId, {method: "GET"}).then(function(response) {
        return response.json();    
      }).then(function(myJson) {
        if (myJson.response){
            camera = myJson.response[0];

            populateCameraModal(camera)
        }
  
      });
}



/**
 * Populate modal dialog with current user information
 */

 function populateUserModal(user){

  
    document.getElementById("input-user-name").value = user["name"];
    document.getElementById("input-user-surname").value = user["surname"];
    document.getElementById("input-user-email").value = user["mail"];
    document.getElementById("input-user-pass").value = "";
    document.getElementById("input-user-pass-repeat").value = "";

    document.getElementById("modal-users-save").setAttribute("onclick", "checkFieldsForSubmittingUser(true, "+user["id"]+")");
    
    document.getElementById("modal-users-save").innerHTML = "Edit user";
    document.getElementById("modal-user-title").innerHTML = "Edit user"
  
  
  }


 /**
 * Populate modal dialog with current camera information
 */

  function populateCameraModal(camera){

  
    document.getElementById("input-cameras-name").value = camera["name"];
    document.getElementById("input-cameras-address").value = camera["addr"];
    document.getElementById("input-cameras-port").value = camera["port"];

    document.getElementById("modal-cameras-save").setAttribute("onclick", "checkFieldsForSubmittingCamera(true, "+camera["idCamera"]+")");
    
    document.getElementById("modal-cameras-save").innerHTML = "Edit camera";
    document.getElementById("modal-cameras-title").innerHTML = "Edit camera"
  
  
  } 





  /** 
 * Event to clear the user's modal on hide 
 */
   $('#modal-users').on('hidden.bs.modal', function () {
  
    document.getElementById("input-user-name").value = "";
    document.getElementById("input-user-surname").value = "";
    document.getElementById("input-user-email").value = "";
    document.getElementById("input-user-pass").value = "";
    document.getElementById("input-user-pass-repeat").value = "";
  
    document.getElementById("modal-users-save").setAttribute("onclick", "checkFieldsForSubmittingUser(false, 0)");
    document.getElementById("modal-users-save").innerHTML = "Create user";
    document.getElementById("modal-user-title").innerHTML = "Add new user"

  });



   /** 
 * Event to clear the cameras' modal on hide 
 */
    $('#modal-cameras').on('hidden.bs.modal', function () {
  
        document.getElementById("input-cameras-name").value = "";
        document.getElementById("input-cameras-address").value = "";
        document.getElementById("input-cameras-port").value = "";
      
        document.getElementById("modal-cameras-save").setAttribute("onclick", "checkFieldsForSubmittingCamera(false, 0)");
        document.getElementById("modal-cameras-save").innerHTML = "Create camera";
        document.getElementById("modal-cameras-title").innerHTML = "Add new camera"
    
      });
    
     


  
  /**
   * Opens the uploaded context
   */
   function openUploadContext(){
    document.getElementById("input_img").click();
  }
  
  
  
  /**
  * Updates the thumbnail image with the uploaded image.
  */
  function updateImage(){
    file = document.getElementById("input_img").files[0];
    document.getElementById("patient_new_img").src = URL.createObjectURL(file);
  }
  
  
  function logOut(){

    fetch(api_uri+'logOut/', {method: "GET"
  }).then(function(response) {
      return response.json();
    }).then(function(myJson) {
      window.location.replace("../index.html")
  
  });
    
  
  }
  