-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 03, 2022 at 05:49 AM
-- Server version: 10.4.21-MariaDB
-- PHP Version: 8.0.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `tracking`
--

-- --------------------------------------------------------

--
-- Table structure for table `cameras`
--

CREATE TABLE `cameras` (
  `idCamera` int(11) NOT NULL,
  `addr` varchar(20) COLLATE ucs2_spanish_ci NOT NULL,
  `port` int(11) NOT NULL,
  `name` varchar(100) COLLATE ucs2_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=ucs2 COLLATE=ucs2_spanish_ci;

--
-- Dumping data for table `cameras`
--

INSERT INTO `cameras` (`idCamera`, `addr`, `port`, `name`) VALUES
(2, '127.0.0.1', 5002, 'Main camera'),
(5, '127.0.0.1', 40, 'Other camera');

-- --------------------------------------------------------

--
-- Table structure for table `configparams`
--

CREATE TABLE `configparams` (
  `id` int(11) NOT NULL,
  `recordAnalisisPath` varchar(512) COLLATE ucs2_spanish_ci NOT NULL,
  `recordWebPath` varchar(512) COLLATE ucs2_spanish_ci NOT NULL,
  `imageDBPath` varchar(512) COLLATE ucs2_spanish_ci NOT NULL,
  `imageWebPath` varchar(512) COLLATE ucs2_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=ucs2 COLLATE=ucs2_spanish_ci;

--
-- Dumping data for table `configparams`
--

INSERT INTO `configparams` (`id`, `recordAnalisisPath`, `recordWebPath`, `imageDBPath`, `imageWebPath`) VALUES
(1, 'trackingsystemforchildren/server/records/', 'C:/xampp/htdocs/web/APP/records/', 'C:/users/theidel/trackingsystemforchildren/server/db/', 'C:/xampp/htdocs/web/app/img/patients/');

-- --------------------------------------------------------

--
-- Table structure for table `patient`
--

CREATE TABLE `patient` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE ucs2_spanish_ci NOT NULL,
  `surname` varchar(255) COLLATE ucs2_spanish_ci NOT NULL,
  `contactPerson` varchar(255) COLLATE ucs2_spanish_ci NOT NULL,
  `contact` varchar(80) COLLATE ucs2_spanish_ci NOT NULL,
  `userInCharge` int(11) NOT NULL,
  `pathology` varchar(100) COLLATE ucs2_spanish_ci NOT NULL,
  `img` varchar(255) COLLATE ucs2_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=ucs2 COLLATE=ucs2_spanish_ci;

--
-- Dumping data for table `patient`
--

INSERT INTO `patient` (`id`, `name`, `surname`, `contactPerson`, `contact`, `userInCharge`, `pathology`, `img`) VALUES
(1, 'Adrian', 'Garcia', 'Paco Martinez', '+34 60112367', 3, 'Test pathology', '1.jpg'),
(2, 'Michel', 'Douglas', 'Sara Olivares', '+34 605112531', 2, 'Test pathology', '2.jpg'),
(3, 'Maia', 'Amiran', 'Arthur Robinson', '+34 603113546', 3, 'Test pathology', '3.jpg'),
(4, 'John', 'Doe', 'Luke Doe', '+34 530143554', 1, 'Test pathology', ''),
(51, 'Marta', 'Andreu', 'wdsad', '31e3wdw', 2, '1ddw', ''),
(52, 'Laura', 'Martinez', 'Yo mismo', '01928392', 2, 'Sin patologias detectadas', ''),
(53, 'Francisco', 'Rodriguez', 'Yo mismo', '12312321', 2, 'Sin patologias detectadas', ''),
(54, 'Alex', 'Miranda', '8127389', 'suasdas', 2, 'Y su patologia', ''),
(55, 'Laia', 'Garcia', 'Franco', '66666666akjn', 2, 'Pues no es muy listo', ''),
(56, 'Emily', 'Rose', 'adjkajkdas', '821iow', 3, 'iakslmd´kaslmdl', '56.jpg'),
(57, 'Joseph', 'Silva', 'asdasd', 'skdamlkdsmlk21', 2, '31rik', '57.jpg'),
(58, 'Tony', 'Blair', 'Nadie', '1092i3912', 2, 'apsdjklasjlkdaskldasnkjlasd', '58.jpeg'),
(73, 'test name', 'test surname', 'test person', '333 444 555', 2, 'test pathology', '73.jpg'),
(74, 'test name', 'test surname', 'test person', '000 111 222', 2, 'test pathology', '74.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `sessions`
--

CREATE TABLE `sessions` (
  `id` int(11) NOT NULL,
  `idPatient` int(11) NOT NULL,
  `sessionDate` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=ucs2 COLLATE=ucs2_spanish_ci;

--
-- Dumping data for table `sessions`
--

INSERT INTO `sessions` (`id`, `idPatient`, `sessionDate`) VALUES
(16, 1, '2022-08-01'),
(17, 1, '2022-08-05'),
(18, 1, '2022-08-07'),
(20, 1, '2022-08-09'),
(32, 1, '2022-08-19'),
(33, 1, '2022-08-19'),
(34, 1, '2022-08-21'),
(35, 56, '2022-08-25'),
(36, 1, '2022-08-26'),
(37, 1, '2022-08-26'),
(38, 1, '2022-08-26'),
(39, 1, '2022-08-26'),
(40, 1, '2022-08-27'),
(41, 1, '2022-09-03'),
(42, 1, '2022-09-03');

-- --------------------------------------------------------

--
-- Table structure for table `sessionsresults`
--

CREATE TABLE `sessionsresults` (
  `id` int(11) NOT NULL,
  `idSession` int(11) NOT NULL,
  `totalFrames` int(11) NOT NULL,
  `analyzedFrames` int(11) NOT NULL,
  `blockSize` int(11) NOT NULL,
  `happy` int(11) NOT NULL,
  `disgust` int(11) NOT NULL,
  `fear` int(11) NOT NULL,
  `sad` int(11) NOT NULL,
  `surprise` int(11) NOT NULL,
  `neutral` int(11) NOT NULL,
  `angry` int(11) NOT NULL,
  `idCamera` int(11) NOT NULL,
  `record` varchar(510) COLLATE ucs2_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=ucs2 COLLATE=ucs2_spanish_ci;

--
-- Dumping data for table `sessionsresults`
--

INSERT INTO `sessionsresults` (`id`, `idSession`, `totalFrames`, `analyzedFrames`, `blockSize`, `happy`, `disgust`, `fear`, `sad`, `surprise`, `neutral`, `angry`, `idCamera`, `record`) VALUES
(3, 16, 68, 65, 30, 0, 0, 0, 0, 0, 2, 1, 2, ''),
(4, 17, 68, 65, 30, 0, 0, 0, 0, 0, 3, 0, 2, ''),
(5, 18, 68, 65, 30, 0, 0, 3, 0, 0, 0, 0, 2, ''),
(6, 20, 68, 65, 30, 0, 0, 0, 0, 0, 3, 0, 2, ''),
(7, 20, 68, 65, 30, 0, 0, 0, 0, 1, 2, 0, 5, ''),
(18, 32, 30, 30, 30, 0, 0, 0, 0, 0, 0, 1, 2, 'records/19_8_2022/record_19_8_2022_11_14_38_1_.mp4'),
(19, 33, 61, 54, 30, 0, 0, 0, 0, 0, 2, 0, 2, 'records/19_8_2022/record_19_8_2022_11_27_7_1_.mp4'),
(20, 33, 61, 60, 30, 0, 0, 0, 0, 1, 1, 0, 5, 'records/19_8_2022/record_19_8_2022_11_27_7_1_.mp4'),
(21, 34, 135, 122, 30, 0, 0, 1, 0, 0, 4, 0, 2, 'records/21_8_2022/record_21_8_2022_19_10_6_1_.mp4'),
(22, 35, 141, 15, 30, 0, 0, 0, 0, 0, 0, 1, 2, 'records/25_8_2022/record_25_8_2022_14_9_13_56_.mp4'),
(23, 36, 197, 189, 30, 0, 0, 1, 0, 0, 2, 4, 2, 'records/26_8_2022/record_26_8_2022_17_26_9_1_.mp4'),
(24, 37, 197, 189, 30, 0, 0, 1, 0, 0, 2, 4, 2, 'records/26_8_2022/record_26_8_2022_17_26_9_1_.mp4'),
(25, 38, 197, 189, 30, 0, 0, 1, 0, 0, 2, 4, 2, 'records/26_8_2022/record_26_8_2022_17_26_9_1_.mp4'),
(26, 39, 197, 189, 30, 0, 0, 1, 0, 0, 2, 4, 2, 'records/26_8_2022/record_26_8_2022_17_26_9_1_.mp4'),
(27, 40, 249, 241, 30, 3, 0, 0, 0, 0, 6, 0, 2, 'records/27_8_2022/record_27_8_2022_15_42_21_1_.mp4'),
(28, 41, 71, 49, 30, 0, 0, 0, 0, 0, 2, 0, 2, 'records/3_9_2022/record_3_9_2022_10_50_23_1_2_.mp4'),
(29, 42, 74, 56, 30, 0, 0, 0, 0, 0, 2, 0, 5, 'records/3_9_2022/record_3_9_2022_11_30_45_1_5_.mp4');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE ucs2_spanish_ci NOT NULL,
  `surname` varchar(255) COLLATE ucs2_spanish_ci NOT NULL,
  `mail` varchar(80) COLLATE ucs2_spanish_ci NOT NULL,
  `pass` varchar(255) COLLATE ucs2_spanish_ci NOT NULL,
  `inactive` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=ucs2 COLLATE=ucs2_spanish_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `surname`, `mail`, `pass`, `inactive`) VALUES
(1, 'admin', '', 'adgaran@epsg.upv.es', '1234', 0),
(2, 'Antonio', 'Martinez', 'newtestmail@gmail.com', 'thisIsMyPassword', 0),
(3, 'Paquita', 'Salas', 'emaildeprueba@gmail.com', '1234', 0),
(4, 'prueba', 'apellido', 'lksadklas@gmail.com', '567', 1),
(6, 'otro usuario', 'prueba', 'asdasdassa', '123', 1),
(10, 'test name', 'test surname', 'new_email@email.com', '1234', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `cameras`
--
ALTER TABLE `cameras`
  ADD PRIMARY KEY (`idCamera`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Indexes for table `configparams`
--
ALTER TABLE `configparams`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `patient`
--
ALTER TABLE `patient`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `sessions`
--
ALTER TABLE `sessions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sessionsresults`
--
ALTER TABLE `sessionsresults`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `cameras`
--
ALTER TABLE `cameras`
  MODIFY `idCamera` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `configparams`
--
ALTER TABLE `configparams`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `patient`
--
ALTER TABLE `patient`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=75;

--
-- AUTO_INCREMENT for table `sessions`
--
ALTER TABLE `sessions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;

--
-- AUTO_INCREMENT for table `sessionsresults`
--
ALTER TABLE `sessionsresults`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
