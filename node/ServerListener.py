import socket
import glob
import json
import time

from threading import Thread
from Recorder import Recorder

from ServerSender import ServerSender


class ServerListener:
    
    BUFF_SIZE = 1024
    CMD_START = "START"
    CMD_STOP = "STOP"
    
    userID = 0
    filePath = None

    port = 0
    server = None
    isRunning = False

    recorder = None
    serverSender = None


    def __init__(self, port):
        
        self.port = port
        self.isRunning = True

        self.recorder = Recorder()
        

    # Starts recording
    # IMPORTANT: Should be executed in parallel
    def startRecording(self):
        self.filePath = self.recorder.resetRecording(self.userID)
        self.recorder.startRecording()
    
    # Stops the recording
    def stopRecording(self):
        self.recorder.stopRecording()


    # Starts listening from the server in loop
    def startListening(self):

        self.server = socket.socket()
        self.server.bind(("0.0.0.0", self.port)) # Listens for server commands by the port in config file
        self.server.listen(5)
        print(f"[*]Nodes server listening...")

        while(self.isRunning):
            
            serverSocket, address = self.server.accept() 
            print(f"[+] {address} is connected.")

            received = serverSocket.recv(self.BUFF_SIZE).decode()
            print(received+" \"Received!!!\"")
        
            receivedArray = received.split("_")
            cmd = receivedArray[0]
            userID =receivedArray[1]
            self.userID = userID

            if(cmd == self.CMD_START):
                t1 = Thread(target=self.startRecording, args=[])
                t1.start()

            if(cmd == self.CMD_STOP): 
                
                self.recorder.stopRecording() # Stops the record and its thread
                t1.join()
                
                time.sleep(2)

                self.uploadRecordToServer() # Upload the record to the server
            


    # Performs an SFTP Secure connection with the server addressed on 
    # the config file and sends the current recording file
    def uploadRecordToServer(self):           
        serverSender = ServerSender()

        f = open("config.json", 'r')
        fJSON = json.load(f)

        #serverParams = fJSON["server_real"]
        serverParams = fJSON["server_test"]
                
        addr = serverParams["addr"]
        port = serverParams["port"]
        user = serverParams["user"]
        #keyPath = serverParams["keyPath"]
        password = serverParams["pass"]

        serverSender.openConnection(addr, int(port), user, password=password)
        serverSender.sendToServer(self.filePath) # Sends the data and closes the connection
                    




    # Stops listening commands from the server
    # IMPORTANT: Should be executed in parallel to be able to finish the listening loop    
    def stopListening(self):

        self.isRunning = False
        self.server.close()
        