import socket
import time
import pysftp
import os


class ServerSender:

    server = None

    def openConnection(self, serverAddr, serverPort, user, pKey):

        self.server = pysftp.Connection(host=serverAddr, username=user, private_key=pKey, port=serverPort)
        print("Not opening")
    
    def openConnection(self, serverAddr, serverPort, user, password):

        self.server = pysftp.Connection(host=serverAddr, username=user, password=password, port=serverPort)
        print("Opening connection!!!")

    
    
    def sendToServer(self, filePath):

        print("FILE: ", filePath)

        fileSplits = filePath.split("/")
        fileName = fileSplits[len(fileSplits)-1]
        

        # 1. Upload the record to be able to be processed (this record is temporal and will be deleted)
        #storePath = "trackingsystemforchildren/server/records/"+fileSplits[len(fileSplits)-2]

        if "records" not in self.server.listdir("C:/Users/adrian/adri/Installation/trackingsystemforchildren/server/"):
            self.server.mkdir("C:/Users/adrian/adri/Installation/trackingsystemforchildren/server/records")

        storePath = "C:/Users/adrian/adri/Installation/trackingsystemforchildren/server/records/"+fileSplits[len(fileSplits)-2]
        if( not self.server.exists(storePath)): self.server.mkdir(storePath)

        self.server.put(filePath, remotepath=storePath+"/"+fileName)
        print(fileName, "sent!")

        # 2. Upload the record to the web folder to be able to be visualized (this record will be permanent)
        storePath = "C:/xampp/htdocs/web/APP/records/"+fileSplits[len(fileSplits)-2]
        if( not self.server.exists(storePath)): self.server.mkdir(storePath)
        self.server.put(filePath, remotepath=storePath+"/"+fileName)
        print(fileName, "stored on web app!")


        self.closeConnection()

        os.remove(filePath) # Once used, it's removed

    def closeConnection(self):
        self.server.close()

