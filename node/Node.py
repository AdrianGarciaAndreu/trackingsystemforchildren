import json

from ServerListener import ServerListener

class Node:
    
    serverAddr = 0
    serverPortToSend = 0
    serverPortToListen = 0

    serverListener= None


    def __init__(self):

        f = open("config.json", 'r')
        fJSON = json.load(f)


        self.serverPortToListen = fJSON["local"]["port"]
                
        self.serverListener = ServerListener(self.serverPortToListen)
        self.serverListener.startListening()



if __name__ == "__main__":
    node = Node()