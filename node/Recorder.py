from cgi import test
import cv2
import numpy as np
import datetime
import os
import mysql.connector
import json


class Recorder:
    cap = None
    out = None
    f_witdh = None
    f_height = None
    
    flag_stop_record = False

    def __init__(self):
        self.cap = cv2.VideoCapture(0)

        
        self.cap.set(3, 1920)
        self.cap.set(4, 1080)

        if (self.cap.isOpened() == False): print("Unable to read camera feed")
        
        frame_width = int(self.cap.get(3))
        frame_height = int(self.cap.get(4))
        
        self.f_witdh = frame_width
        self.f_height = frame_height


    def getCamfromDB(self):
        
        result = 0

        # Read configuration from JSON
        
        f = open("config.json", 'r')
        fJSON = json.load(f)
        serverAddr = fJSON["server_test"]["addr"]
        camName = fJSON["local"]["name"]


        cnx = mysql.connector.connect(user='testUser', password='1234', host=serverAddr, database='tracking')
        mysqlCursor = cnx.cursor()


        sql = "SELECT * FROM Cameras WHERE name LIKE '"+camName+"' "

        mysqlCursor.execute(sql)
        myResult = mysqlCursor.fetchall()

        for row in myResult:
            result = row[0] # Camera id
            
        cnx.close()

        return result  # returns the obtained camera ID
    



    # Resets the recording for a specific user
    def resetRecording(self, userID):
        self.flag_stop_record = False
        self.cap = cv2.VideoCapture(0)
        
        self.cap.set(3, 1920)
        self.cap.set(4, 1080)

        if (self.cap.isOpened() == False): print("Unable to read camera feed")
        

        now = datetime.datetime.now()

        #if the folder doesn't exist, its created
        path = 'records/'+str(now.day)+'_'+str(now.month)+'_'+str(now.year)+'/'
        os.makedirs(path, exist_ok=True)

        # Retrieve the camera ID from th DB

        camID = self.getCamfromDB()

        # Output of the video (codec and format)
        filePath = 'records/'+str(now.day)+'_'+str(now.month)+'_'+str(now.year)
        filePath = filePath +'/record_'+str(now.day)+'_'+str(now.month)+'_'+str(now.year)
        filePath = filePath +'_'+str(now.hour)+'_'+str(now.minute)+'_'+str(now.second)+'_'+str(userID)
        filePath = filePath + '_' +str(camID)+'_.mp4'
        

        vid_cod = cv2.VideoWriter_fourcc(*'avc1')
        self.out = cv2.VideoWriter(filePath,vid_cod, 30, (self.f_witdh,self.f_height))
        
        return filePath




    # Starts the recording
    def startRecording(self):
        while(True):
            try:
                ret, frame = self.cap.read()
                if ret == True and self.flag_stop_record == False: 
                    self.out.write(frame)
                    cv2.imshow('frame',frame)

                    # Press Q on keyboard to stop recording
                    if cv2.waitKey(10) & 0xFF == ord('q'): 
                        self.stop_recording()
                        break

                # Stops the recording
                else: 
                    self.clearRecording()
                    break  
            except Exception as e:
                print("Fail reading!", e)


    # Stops the recording loop
    def stopRecording(self):
        print("Stopping")
        self.flag_stop_record = True


    # Clear recording parameters a free the resources
    def clearRecording(self):
        try:
            self.cap.release()
            self.out.release()
            cv2.destroyAllWindows()

        except:
            print("Fail cleaning!")