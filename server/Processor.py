from calendar import c
import cv2
from matplotlib.style import use
import numpy as np
import math


from Recognizer import Recognizer
from Analyzer import Analyzer

class Processor:

    userId = None
    record = None
    padding = None
    confidence = None
    
    recognizer = None
    analyzer = None
    connectionManager = None

    def __init__(self, userId, record, padding, confidence):
        self.userId = userId
        self.record = record
        self.padding = padding
        self.confidence = confidence

        self.recognizer = Recognizer()
        #self.connectionManager = ConnectionManager()


    # Apply CLAHE equalization to improve contrast 
    # for emotion detection improves the result drastically
    def applyCLAHE(self, frame):
        
        lab= cv2.cvtColor(frame, cv2.COLOR_BGR2LAB)
        l_channel, a, b = cv2.split(lab)

        clahe = cv2.createCLAHE(clipLimit=2.0, tileGridSize=(8,8)) 
        cl = clahe.apply(l_channel) # Applying CLAHE to L-channel

        limg = cv2.merge((cl,a,b)) # merge the CLAHE enhanced L-channel with the a and b channel

        enhanced_img = cv2.cvtColor(limg, cv2.COLOR_LAB2BGR) # Return to the original color space
        
        return(enhanced_img)



    def process(self):
        framesToAnalyze = []
        video = cv2.VideoCapture(self.record)

        fps = video.get(cv2.CAP_PROP_FPS)
        totalFrames = video.get(cv2.CAP_PROP_FRAME_COUNT)

        print("Recognizing...")

        while(True):
            success, fr = video.read()
            if(success and (np.shape(fr) != ())):
                
                someone, faces = self.recognizer.recognizeUser(self.userId, fr, 80, 0.4)
                if someone: 
                    for face in faces:
                        faceImproved = self.applyCLAHE(face) # Preprocess the image to enhance the result
                        framesToAnalyze.append(faceImproved)
            else: break
        
        analyzedFrames = len(framesToAnalyze)
        
        blockSize = math.ceil(fps) # 1 second
        #blockSize = 5 # JUST FOR TEST

        self.analyzer = Analyzer(framesToAnalyze,blockSize)
        classification = self.analyzer.analyze()

        return (totalFrames, analyzedFrames, blockSize, classification)





    
