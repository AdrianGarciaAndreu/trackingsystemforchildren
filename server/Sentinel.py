from concurrent.futures import process
import datetime
import time
import glob
import json
import mysql.connector
import os

from numpy import record

from Processor import Processor

class Sentinel():

    def mainLoop(self):

        processed = False

        # Current date and time
        now = datetime.datetime.now()
        formattedDate = now.strftime('%Y-%m-%d')

        limit_hour = 10 #TODO: read from the database (20:00)

        recordsPath = "records/"+str(now.day)+"_"+str(now.month)+"_"+str(now.year)+"/"


        while(True):
            if(now.hour>= limit_hour and not processed): # Search all the videos and process it
                
                print("Waiting for files!")
                
                files = glob.glob(recordsPath+"/*.mp4")
                
                for file in files:
                    recordData = file.split("_")
                    userId = recordData[9]
                    camId = recordData[10]

                    file = file.replace("\\","/")
                    print("Start "+file+" processing!") 
                    
                    fileNameArray = file.split("/")
                    fileName = fileNameArray[len(fileNameArray)-1]

                    # Process the video
                    processor = Processor(userId, file,80,0.4)
                    totalFrames, analyzedFrames, blockSize, results = processor.process()
                    
                    #idCamera = 2 

                    # Format the #results to be stored in a local JSON file
                    results["totalFrames"] = totalFrames
                    results["analyzedFrames"] = analyzedFrames
                    results["blockSize"] = blockSize
                    results["idCamera"] = camId

                    f = open("localResults/"+fileName+".json", 'w')
                    f.write(str(results).replace("'","\""))
                    f.close()


                    f = open("localResults/"+fileName+".json", 'r')
                    fJSON = json.load(f)
                    self.dbUploadSession(fJSON, userId, formattedDate, (file))
                    f.close()
                
                    os.remove(file)

                    processed = True
                time.sleep(10)

            elif(processed): 
                print("Reset processing!")
                processed = False # Reset the processed flag 
            else: 
                print("Waiting...")

                # Updates time
                now = datetime.datetime.now()
                recordsPath = "records/"+str(now.day)+"_"+str(now.month)+"_"+str(now.year)+"/"

                time.sleep(10) # Waits until the limit hour


    def dbUploadSession(self, jsonData, userId, formattedDate, filePath):
        # Connect with DB
        cnx = mysql.connector.connect(user='root', password='', host='127.0.0.1', database='tracking')
        
        print("Succesfully connected")
        mysqlCursor = cnx.cursor()
        
        sql = "INSERT INTO sessions(id, idPatient, sessionDate) VALUES (%s, %s, %s)"
        values = ("NULL", userId, formattedDate)
        
        mysqlCursor.execute(sql, values)
        cnx.commit()

        recordUploaded = mysqlCursor.rowcount
        print("1 session created: "+str(recordUploaded))

        if(recordUploaded>0): # Add the details of the session
            
            sqlB = "INSERT INTO sessionsResults(idSession, totalFrames, analyzedFrames, blockSize, happy, disgust, fear, sad, surprise, neutral, angry, idCamera, record) "
            sqlB = sqlB + "VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)"


            valuesB = (mysqlCursor.lastrowid, jsonData["totalFrames"], jsonData["analyzedFrames"], jsonData["blockSize"], jsonData["happy"],
            jsonData["disgust"], jsonData["fear"], jsonData["sad"], jsonData["surprise"], jsonData["neutral"], 
            jsonData["angry"], jsonData["idCamera"], filePath)


            mysqlCursor.execute(sqlB, valuesB)
            cnx.commit()
            recordUploaded = mysqlCursor.rowcount
            if(recordUploaded<1): print("Insertion FAILED!!") 

        cnx.close()

