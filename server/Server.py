import threading
import json

#from NodeListener import NodeListener
from Sentinel import Sentinel


class Server:
    sentinel = None
    nodeListener = None

    def startServer(self):
        threadSentinel = threading.Thread(target=self.startSentinel)
        #threadNodeListener = threading.Thread(target=self.startNodeListener)

        # Process 1.
        threadSentinel.start()

        # Process 2.
        #threadNodeListener.start()



    def startSentinel(self):
        print("Starting sentinel...")
        self.sentinel = Sentinel()
        self.sentinel.mainLoop()



if __name__ == "__main__":
    server = Server()
    server.startServer()