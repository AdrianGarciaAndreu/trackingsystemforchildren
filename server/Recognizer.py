
from collections import Counter
import math
#from types import NoneType
import numpy as np
import cv2

import matplotlib.pyplot as plt
import pandas as pd

from deepface import DeepFace
from retinaface import RetinaFace
from rsa import verify



class Recognizer():
    
    metrics = ["cosine", "euclidean", "euclidean_l2"]
    models = ["VGG-Face", "Facenet", "Facenet512", "OpenFace", "DeepFace", "DeepID", "ArcFace", "Dlib", "SFace"]
    tmp_path = 'tmp/'
    db_path = 'db/'

    # Model used to detect faces throug OpenCV DNN
    modelFile = "models/res10_300x300_ssd_iter_140000.caffemodel"
    configFile = "models/deploy.prototxt.txt"    
    net = None

    def __init__(self):
        self.net = cv2.dnn.readNetFromCaffe(self.configFile, self.modelFile)
        
        self.net.setPreferableBackend(cv2.dnn.DNN_BACKEND_CUDA)
        self.net.setPreferableTarget(cv2.dnn.DNN_TARGET_CUDA)


    def recognizeUser(self, userId, frame, padding, confidenceTh):
        result = False
        croppedFrames = []
        processedFrame = frame

        #Construcción del BLOB (Mejores resultados con BGR que RGB)
        # Convert image to blob (better with BGR than RGB images)
        h, w = frame.shape[:2]
        
        #blob = cv2.dnn.blobFromImage(cv2.resize(frame, (300, 300)), 1.0, (300, 300), (104.0, 117.0, 123.0))
        blob = cv2.dnn.blobFromImage(cv2.resize(frame, (300, 300)), 1.0, (300, 300), (104.0, 117.0, 123.0))

        self.net.setInput(blob) # Send the image to the CNN
        faces = self.net.forward() # Get the results

        for face in range(faces.shape[2]):
            confidence = faces[0,0,face,2]
            
            if confidence>confidenceTh:

                box = faces[0, 0, face, 3:7] * np.array([w, h, w, h])
                (x, y, x1, y1) = box.astype("int")
                #cv2.rectangle(img, (x, y), (x1, y1), (0, 0, 255), 5)
                    
                # Calculate padding to give some space to the face cropping if possible
                fy=y-padding+1
                fx=x-padding+1
                fw=x1+padding
                fh=y1+padding
                
                padding_limits = False
                
                if((y-padding+1)<0): padding_limits = True #fy = facey+1
                if((x-padding+1)<0): padding_limits = True #fx = facex+1
                if((y1+padding)>(h)): padding_limits = True #fh = facey+faceh
                if((x1+padding)>(w)): padding_limits = True #fw = facex+facew

                # Crop the image
                if (padding_limits == False): processedFrame = frame[fy:fh, fx:fw]
                else: processedFrame = frame[y:y1, x:x1]

            else: 
                processedFrame = frame 
                
            result = self.verifyUser(userId, processedFrame)
            if(result): croppedFrames.append(processedFrame)
                
            return (result, croppedFrames)
            
    

    def verifyUser(self, userId, frame):
            
        cv2.imwrite(self.tmp_path+'frame_tmp.jpg', frame)
        path_ad = self.tmp_path+"frame_tmp.jpg"

        df = DeepFace.verify(img1_path = path_ad, img2_path = 'db/'+str(userId)+'.jpg', 
        model_name = self.models[2], distance_metric = self.metrics[2], enforce_detection = False, detector_backend="retinaface")
        
        #print("\n",df, "\n" )

        if(df['verified']):  return(True)
        else: return(False)