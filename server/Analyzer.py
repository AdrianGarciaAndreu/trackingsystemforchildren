
from collections import Counter
import math
#from types import NoneType
import numpy as np
import cv2

import matplotlib.pyplot as plt
import pandas as pd


from deepface import DeepFace
from retinaface import RetinaFace


class Analyzer():
    
    framesToProcess = []
    total_clases = None
    blockSize = 30

    def __init__(self, framesToProcess, blockSize):
        self.total_clases = {"happy":0, "disgust":0, "fear":0, "sad":0, "surprise":0, "neutral":0, "angry":0}
        self.framesToProcess = framesToProcess
        self.blockSize = blockSize


    # Classify one block emotions and return them as a dictionary
    def summarizeBlock(self, data):
        block_total_clases = {"happy":0, "disgust":0, "fear":0, "sad":0, "surprise":0, "neutral":0, "angry":0}
        emotions, counts = np.unique(data, return_counts=True)

        for idx, emotion in np.ndenumerate(emotions):
            block_total_clases[emotion] = (block_total_clases[emotion] + counts[idx])

        return block_total_clases


    # Get the emotion that has more occurences in a block
    def addEmotionToResult(self, block_data):
        block_dominant_emotion = max(block_data, key=block_data.get) 
        
        # Add one unit to the total count of emotions appeared in the analyzed video
        self.total_clases[block_dominant_emotion] = self.total_clases[block_dominant_emotion] + 1


    # Scale a frame to the desired percentage
    def scaleFrame(self, frame, percentage):
        scale_percent = percentage
        width = int(frame.shape[1] * scale_percent / 100)
        height = int(frame.shape[0] * scale_percent / 100)

        dsize = (width, height)

        returnFrame = cv2.resize(frame, dsize) # scale the iamge to work better
        return(returnFrame)




    
    def analyze(self):
        block_data = {"happy":0, "disgust":0, "fear":0, "sad":0, "surprise":0, "neutral":0, "angry":0}
        
        block_size = self.blockSize

        current_block_index = 0
        current_total_index = 0

        current_counter = 0

        final_frame = False
        success = True
        
        while True:
            
            block_processed = True
            block_results = [] # resultados del bloque actual
            current_block_index = 0

            while(current_block_index<block_size and not final_frame): # INICIO DE BLOQUE #################################
                block_processed = False

                if(current_counter>=len(self.framesToProcess)): success = False
                else: success = True

                if(success): #and current_total_index<DEBUG_LIMIT):
                    frame = self.framesToProcess[current_counter]
                    ##################
                    # Analisis del Frame
                    ##################
                    frameToProcess = frame # variable a processar por la CNN
                    try:
                        block_results.append(DeepFace.analyze(frameToProcess, detector_backend = 'retinaface', 
                        enforce_detection = False, actions = ["emotion"])['dominant_emotion'])

                        # Colecta los datos del bloque y los suma al computo total
                        block_data = self.summarizeBlock(block_results)
                        print("Current block: ",block_data)
                        

                    except: block_results = "No person detected"

                    ##################
                    # Fin del análisis del FRAME
                    ##################

                    current_block_index = current_block_index + 1
                    current_counter = current_counter + 1

                else:
                    final_frame = True
                    if(current_block_index == block_size or current_block_index == 0):
                        block_processed = True  

                    break
                # FIN DE BLOQUE ################################################################### 

            if(not block_processed):
                block_processed = True
                self.addEmotionToResult(block_data)
                print("TOTAL block finished: ", self.total_clases)

            if(final_frame): 
                print("No more to read!")
                break
            else: current_total_index = current_total_index + current_block_index
            

        return self.total_clases